﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class MP_Pasaje
    {
        Acceso acceso = new Acceso();

        public void Insertar(BE.Pasaje p)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id_pasajero", p.Pasajero.Id));
            parametros.Add(acceso.CrearParametro("@id_vuelo", p.Vuelo.Id));
            acceso.Escribir("sp_pasajes_insertar", parametros);

            acceso.Cerrar();
        }

        //public List<BE.Pasaje> Listar()
        //{
        //    List<BE.Pasaje> pasajes = new List<BE.Pasaje>();

        //    acceso.Abrir();

        //    DataTable tabla = acceso.Leer("sp_pasajes_listar");

        //    foreach (DataRow registro in tabla.Rows)
        //    {
        //        BE.Pasaje p = new BE.Pasaje();
        //        p.Id = int.Parse(registro[0].ToString());
        //        p.Pasajero.Nombre = registro[1].ToString();
        //        p.Pasajero.Apellido = registro[2].ToString();
        //        p.Pasajero.Dni = int.Parse(registro[3].ToString());
        //        p.Vuelo.Destino.Nombre = registro[4].ToString();
        //        p.Vuelo.Destino.Pais = registro[5].ToString();
        //        p.Vuelo.Partida = DateTime.Parse(registro[6].ToString());
        //        p.Vuelo.Llegada = DateTime.Parse(registro[7].ToString());
        //        p.Vuelo.Tipo = registro[8].ToString();
        //        pasajes.Add(p);
        //    }

        //    acceso.Cerrar();

        //    return pasajes;
        //}


        public DataTable Listar()
        {
            acceso.Abrir();
            DataTable dtPasajes = acceso.Leer("sp_pasajes_listar");

            acceso.Cerrar();
            return dtPasajes;
        }
    }
}
