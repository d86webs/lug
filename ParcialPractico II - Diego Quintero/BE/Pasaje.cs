﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Pasaje
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private Pasajero pasajero = new Pasajero();

        public Pasajero Pasajero
        {
            get { return pasajero; }
            set { pasajero = value; }
        }

        private Vuelo vuelo = new Vuelo();

        public Vuelo Vuelo
        {
            get { return vuelo; }
            set { vuelo = value; }
        }
    }
}
