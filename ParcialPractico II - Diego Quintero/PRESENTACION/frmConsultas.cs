﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmConsultas : Form
    {
        BLL.Destino gestorDestinos = new BLL.Destino();
        BLL.Vuelo gestorVuelos = new BLL.Vuelo();
        public frmConsultas()
        {
            InitializeComponent();
        }

        private void frmConsultas_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            dgDestinos.DataSource = null;
            dgResultados.DataSource = null;
            dgDestinos.DataSource = gestorDestinos.ListarDestinos();
            dgDestinos.Columns[0].Visible = false;
            dgDestinos.Columns[3].Visible = false;
            dgDestinos.ClearSelection();
        }

        private void btnListarPorDestino_Click(object sender, EventArgs e)
        {
            BE.Destino destino = (BE.Destino)dgDestinos.SelectedRows[0].DataBoundItem;
            dgResultados.DataSource = null;
            dgResultados.DataSource = gestorVuelos.ListarVuelosPorDestino(destino.Id);
            dgResultados.Columns[0].Visible = false;
            dgResultados.Columns[8].Visible = false;
            dgDestinos.ClearSelection();
        }

        private void dgDestinos_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnListarPorFechaPartida_Click(object sender, EventArgs e)
        {
            dgResultados.DataSource = null;
            DateTime fecha = dtpFecha.Value.Date;
            dgResultados.DataSource = gestorVuelos.ListarVuelosPorFechaPartida(fecha);
        }

        private void btnListarPorFechaLlegada_Click(object sender, EventArgs e)
        {
            dgResultados.DataSource = null;
            DateTime fecha = dtpFecha.Value.Date;
            dgResultados.DataSource = gestorVuelos.ListarVuelosPorFechaLlegada(fecha);
        }

        private void btnListarVuelosNacionales_Click(object sender, EventArgs e)
        {
            dgResultados.DataSource = null;
            dgResultados.DataSource = gestorVuelos.ListarVuelosNacionales();
        }

        private void btnListarVuelosInternacionales_Click(object sender, EventArgs e)
        {
            dgResultados.DataSource = null;
            dgResultados.DataSource = gestorVuelos.ListarVuelosInternacionales();
        }

        private void dgResultados_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void btnVuelosOcupacionMenorA40_Click(object sender, EventArgs e)
        {
            dgResultados.DataSource = null;
            dgResultados.DataSource = gestorVuelos.ListarVuelosFinalizadosOcupacionMenorA40();
            dgResultados.ClearSelection();
        }
    }
}
