﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Recibo
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private int mes;

        public int Mes
        {
            get { return mes; }
            set { mes = value; }
        }

        private int ano;

        public int Ano
        {
            get { return ano; }
            set { ano = value; }
        }

        private decimal bruto;

        public decimal Bruto
        {
            get { return bruto; }
            set { bruto = value; }
        }

        private decimal neto;

        public decimal Neto
        {
            get { return neto; }
            set { neto = value; }
        }

        private decimal descuento;

        public decimal Descuento
        {
            get { return descuento; }
            set { descuento = value; }
        }
    }
}
