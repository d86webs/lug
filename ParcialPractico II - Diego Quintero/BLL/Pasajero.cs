﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Pasajero
    {
        DAL.MP_Pasajero mp = new DAL.MP_Pasajero();

        public void Insertar(BE.Pasajero p)
        {
            if (p.Id == 0)
            {
                mp.Insertar(p);
            }
            else
            {
                mp.Actualizar(p);
            }
        }

        public void Eliminar(BE.Pasajero p)
        {
            mp.Eliminar(p);
        }

        public List<BE.Pasajero> ListarPasajeros()
        {
            return mp.Listar();
        }
    }
}
