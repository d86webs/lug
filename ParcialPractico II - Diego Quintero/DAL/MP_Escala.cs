﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class MP_Escala
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Escala e)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id_vuelo", e.Id_vuelo));
            parametros.Add(acceso.CrearParametro("@id_destino", e.Id_destino));
            acceso.Escribir("sp_escalas_insertar", parametros);

            acceso.Cerrar();
        }

        public void Actualizar(BE.Escala e)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", e.Id));
            parametros.Add(acceso.CrearParametro("@id_vuelo", e.Id_vuelo));
            parametros.Add(acceso.CrearParametro("@id_destino", e.Id_destino));
            acceso.Escribir("sp_escalas_actualizar", parametros);

            acceso.Cerrar();
        }

        public void Eliminar(BE.Escala e)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", e.Id));
            acceso.Escribir("sp_escalas_eliminar", parametros);

            acceso.Cerrar();
        }

        public DataTable ListarEscalas(int id_vuelo)
        {
            DataTable escalas = new DataTable();

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_vuelo", id_vuelo));
            string SQL = string.Format("sp_escalas_listar");

            escalas = acceso.Leer(SQL, parametros);

            acceso.Cerrar();

            return escalas;
        }
    }
}
