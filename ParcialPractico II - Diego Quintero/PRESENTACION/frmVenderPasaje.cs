﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmVenderPasaje : Form
    {
        BLL.Pasajero gestorPasajero = new BLL.Pasajero();
        BLL.Vuelo gestorVuelo = new BLL.Vuelo();
        BLL.Pasaje gestorPasaje = new BLL.Pasaje();
        public frmVenderPasaje()
        {
            InitializeComponent();
        }

        private void frmVender_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Enlazar();
            LimpiarSeleccion();
        }

        void Enlazar()
        {
            dgPasajeros.DataSource = null;
            dgVuelos.DataSource = null;
            dgPasajes.DataSource = null;

            dgPasajeros.DataSource = gestorPasajero.ListarPasajeros();
            dgVuelos.DataSource = gestorVuelo.ListarVuelo();
            dgPasajes.DataSource = gestorPasaje.Listar();

            dgPasajeros.Columns[0].Visible = false;
            dgVuelos.Columns[0].Visible = false;
            dgPasajes.Columns[0].Visible = false;
            dgPasajes.Columns[9].Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnVenderPasaje_Click(object sender, EventArgs e)
        {
            BE.Pasaje p = new BE.Pasaje();
            BE.Pasajero pasajero = (BE.Pasajero)dgPasajeros.SelectedRows[0].DataBoundItem;
            BE.Vuelo vuelo = (BE.Vuelo)dgVuelos.SelectedRows[0].DataBoundItem;
            p.Pasajero.Id = pasajero.Id;
            p.Vuelo.Id = vuelo.Id;
            gestorVuelo.Insertar(p.Vuelo);
            gestorPasaje.Insertar(p);
            Enlazar();
            LimpiarSeleccion();
        }

        void LimpiarSeleccion()
        {
            dgPasajeros.ClearSelection();
            dgVuelos.ClearSelection();
            dgPasajes.ClearSelection();
        }

        private void dgVuelos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Vuelo vuelo = (BE.Vuelo)dgVuelos.SelectedRows[0].DataBoundItem;
            frmEscalasVer frmEscalasVer = new frmEscalasVer();
            frmEscalasVer.EnviarVuelo(vuelo.Id);
            frmEscalasVer.ShowDialog();
        }

        private void dgPasajes_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }
    }
}
