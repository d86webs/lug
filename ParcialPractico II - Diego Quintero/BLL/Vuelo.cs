﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Vuelo
    {
        DAL.MP_Vuelo mp = new DAL.MP_Vuelo();

        public void Insertar(BE.Vuelo v)
        {
            if (v.Id == 0)
            {
                mp.Insertar(v);
            }
            else
            {
                mp.Actualizar(v);
            }
        }

        public void Eliminar(BE.Vuelo v)
        {
            mp.Eliminar(v);
        }

        public List<BE.Vuelo> ListarVuelo()
        {
            return mp.Listar();
        }

        public DataTable ListarVuelosPorDestino(int id)
        {
            return mp.VuelosPorDestinos(id);
        }

        public DataTable ListarVuelosPorFechaPartida(DateTime fecha)
        {
            return mp.VuelosPorFechaPartida(fecha);
        }

        public DataTable ListarVuelosPorFechaLlegada(DateTime fecha)
        {
            return mp.VuelosPorFechaLlegada(fecha);
        }

        public DataTable ListarVuelosNacionales()
        {
            return mp.VuelosNacionales();
        }

        public DataTable ListarVuelosInternacionales()
        {
            return mp.VuelosInternacionales();
        }

        public DataTable ListarVuelosFinalizadosOcupacionMenorA40()
        {
            return mp.VuelosFinalizadosOcupadocionMenorA40();
        }
    }
}
