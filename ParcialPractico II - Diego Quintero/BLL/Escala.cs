﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BLL
{
    public class Escala
    {
        DAL.MP_Escala mp = new DAL.MP_Escala();

        public void Insertar(BE.Escala e)
        {
            if (e.Id == 0)
            {
                mp.Insertar(e);
            }
            else
            {
                mp.Actualizar(e);
            }
        }

        public void Borrar(BE.Escala e)
        {
            mp.Eliminar(e);
        }

        public DataTable ListarEscalas(int id_vuelo)
        {
            return mp.ListarEscalas(id_vuelo);
        }
    }
}
