﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmConceptos : Form
    {
        BLL.Concepto gestor = new BLL.Concepto();
        public frmConceptos()
        {
            InitializeComponent();
        }

        private void frmConceptos_Load(object sender, EventArgs e)
        {
            txtId.Enabled = false;
            txtNombre.Enabled = false;
            txtPorcentaje.Enabled = false;

            Enlazar();
        }

        void Enlazar()
        {
            dgConceptos.DataSource = null;
            dgConceptos.DataSource = gestor.ListarConceptos();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtId.Clear();
            txtNombre.Enabled = true;
            txtNombre.Clear();
            txtPorcentaje.Enabled = true;
            txtPorcentaje.Clear();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtNombre.Text) || string.IsNullOrWhiteSpace(txtPorcentaje.Text))
            {
                MessageBox.Show("Debe completar uno o mas campos!");
            }
            else
            {
                BE.Concepto concepto = new BE.Concepto();
                concepto.Nombre = txtNombre.Text;
                concepto.Porcentaje = decimal.Parse(txtPorcentaje.Text);

                gestor.Insertar(concepto);

                Enlazar();
            }
        }

        private void dgConceptos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgConceptos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNombre.Enabled = true;
            txtPorcentaje.Enabled = true;

            txtId.Text = dgConceptos.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNombre.Text = dgConceptos.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtPorcentaje.Text = dgConceptos.Rows[e.RowIndex].Cells[2].Value.ToString();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text) || string.IsNullOrWhiteSpace(txtPorcentaje.Text))
            {
                MessageBox.Show("Debe completar uno o mas campos!");
            }
            else
            {
                BE.Concepto concepto = new BE.Concepto();
                concepto.Id = int.Parse(txtId.Text);
                concepto.Nombre = txtNombre.Text;
                concepto.Porcentaje = decimal.Parse(txtPorcentaje.Text);

                gestor.Insertar(concepto);

                Enlazar();
            }
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtId.Text))
            {
                MessageBox.Show("Debe seleccionar un concepto!");
            }
            else
            {
                BE.Concepto concepto = new BE.Concepto();
                concepto.Id = int.Parse(txtId.Text);

                gestor.Eliminar(concepto);

                Enlazar();
            }
        }
    }
}