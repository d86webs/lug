﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Mes
    {
        DAL.MP_Mes mp = new DAL.MP_Mes();
        public List<BE.Mes> ListarMeses()
        {
            return mp.Listar();
        }
    }
}
