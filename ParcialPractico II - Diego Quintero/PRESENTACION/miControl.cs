﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class miControl : UserControl
    {
        public miControl()
        {
            InitializeComponent();
        }

        private bool requerido;

        public bool Requerido
        {
            get { return requerido; }
            set { requerido = value; }
        }


        private void miControl_Load(object sender, EventArgs e)
        {

        }

        public bool Validar()
        {
            bool ok = true;

            if (requerido)
            {
                if (string.IsNullOrWhiteSpace(txtCampo.Text))
                {
                    txtCampo.BackColor = Color.MistyRose;
                }
                else
                {
                    txtCampo.BackColor = Color.White;
                }
            }

            return ok;
        }

        public void LlenarCampo(string valor)
        {
            txtCampo.Text = valor;
        }

        public string DevuelvoValor()
        {
            string valor = "";

            valor = txtCampo.Text;

            return valor;
        }

        public void Limpiar()
        {
            txtCampo.Clear();
            txtCampo.BackColor = Color.White;
        }
    }
}
