﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmJuego : Form
    {
        BLL.Juego gestorJuego = new BLL.Juego();
        BLL.Partida gestorPartida = new BLL.Partida();
        BLL.Puntaje gestorPuntaje = new BLL.Puntaje();
        int tiros = 3;
        int dadosElegidos = 0;
        int[] categorias = new int[11];
        int caraDado1 = 0;
        int caraDado2 = 0;
        int caraDado3 = 0;
        int caraDado4 = 0;
        int caraDado5 = 0;
        int[] escalera = new int[5] { 1,2,3,4,5 };
        int[] obtenerEscalera = new int[5];

        public frmJuego()
        {
            InitializeComponent();
        }

        private void btnLanzar_Click(object sender, EventArgs e)
        {
            LanzarDados();
            tiros--;
            lblTirosDisponibles.Text = "TIROS DISPONIBLES: " + tiros.ToString();
            if(tiros == 0 || dadosElegidos == 5)
            {
                CalcularPuntajes();
            }
            
        }

        private void btnDado1_Click_1(object sender, EventArgs e)
        {
            DadoOcultarYDesactivar(btnDado1);
            DadoElegido(caraDado1, btnDado1);
            btnDado1.Visible = false;
            obtenerEscalera[dadosElegidos-1] = caraDado1;
        }

        private void btnDado2_Click(object sender, EventArgs e)
        {
            DadoOcultarYDesactivar(btnDado2);
            DadoElegido(caraDado2, btnDado2);
            btnDado2.Visible = false;
            obtenerEscalera[dadosElegidos-1] = caraDado2;
        }

        private void btnDado3_Click(object sender, EventArgs e)
        {
            DadoOcultarYDesactivar(btnDado3);
            DadoElegido(caraDado3, btnDado3);
            btnDado3.Visible = false;
            obtenerEscalera[dadosElegidos-1] = caraDado3;
        }

        private void btnDado4_Click(object sender, EventArgs e)
        {
            DadoOcultarYDesactivar(btnDado4);
            DadoElegido(caraDado4, btnDado4);
            btnDado4.Visible = false;
            obtenerEscalera[dadosElegidos-1] = caraDado4;
        }

        private void btnDado5_Click(object sender, EventArgs e)
        {
            DadoOcultarYDesactivar(btnDado5);
            DadoElegido(caraDado5, btnDado5);
            btnDado5.Visible = false;
            obtenerEscalera[dadosElegidos-1] = caraDado5;
        }

        void DesactidavarDados(Button b)
        {
            b.Enabled = false;
        }

        void LanzarDados()
        {
            if (btnDado1.Enabled != false && tiros > 0)
            {
                DadoMostrarYActivar(btnDado1);
                caraDado1 = gestorJuego.LanzarDado();
                btnDado1.BackgroundImage = Image.FromFile(".\\Imagenes\\" + caraDado1.ToString() + ".jpg");
            }

            if (btnDado2.Enabled != false && tiros > 0)
            {
                DadoMostrarYActivar(btnDado2);
                caraDado2 = gestorJuego.LanzarDado();
                btnDado2.BackgroundImage = Image.FromFile(".\\Imagenes\\" + caraDado2.ToString() + ".jpg");
            }

            if (btnDado3.Enabled != false && tiros > 0)
            {
                DadoMostrarYActivar(btnDado3);
                caraDado3 = gestorJuego.LanzarDado();
                btnDado3.BackgroundImage = Image.FromFile(".\\Imagenes\\" + caraDado3.ToString() + ".jpg");
            }

            if (btnDado4.Enabled != false && tiros > 0)
            {
                DadoMostrarYActivar(btnDado4);
                caraDado4 = gestorJuego.LanzarDado();
                btnDado4.BackgroundImage = Image.FromFile(".\\Imagenes\\" + caraDado4.ToString() + ".jpg");
            }

            if (btnDado5.Enabled != false && tiros > 0)
            {
                DadoMostrarYActivar(btnDado5);
                caraDado5 = gestorJuego.LanzarDado();
                btnDado5.BackgroundImage = Image.FromFile(".\\Imagenes\\" + caraDado5.ToString() + ".jpg");
            }
        }

        private void frmJuego_Load(object sender, EventArgs e)
        {
            grbMesa.BackColor = Color.FromArgb(10, 118, 30);
            lblTirosDisponibles.Text = "TIROS DISPONIBLES: " + tiros.ToString();
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorPartida.ListarPartida();
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Visible = false;
            BkgImageDados();
            DadosOcultar();
            DadosSeleccionadosOcultar();
        }

        void DadoElegido(int valor, Button dado)
        {
            dadosElegidos++;
            lblDadosSeleccionados.Text = "DADOS SELECCIONADOS: " + dadosElegidos.ToString();

            switch(dadosElegidos)
            {
                case 1:
                    DadoOcultarYDesactivar(dado);
                    btnDadoSel1.BackgroundImage = Image.FromFile(".\\Imagenes\\" + valor.ToString() + ".jpg");
                    DadoSeleccionadoMostrar(btnDadoSel1);
                    categorias[valor] += valor;
                    break;
                case 2:
                    DadoOcultarYDesactivar(dado);
                    btnDadoSel2.BackgroundImage = Image.FromFile(".\\Imagenes\\" + valor.ToString() + ".jpg");
                    btnDadoSel2.Visible = true;
                    categorias[valor] += valor;
                    break;
                case 3:
                    DadoOcultarYDesactivar(dado);
                    btnDadoSel3.BackgroundImage = Image.FromFile(".\\Imagenes\\" + valor.ToString() + ".jpg");
                    btnDadoSel3.Visible = true;
                    categorias[valor] += valor;
                    break;
                case 4:
                    DadoOcultarYDesactivar(dado);
                    btnDadoSel4.BackgroundImage = Image.FromFile(".\\Imagenes\\" + valor.ToString() + ".jpg");
                    btnDadoSel4.Visible = true;
                    categorias[valor] += valor;
                    break;
                case 5:
                    DadoOcultarYDesactivar(dado);
                    btnDadoSel5.BackgroundImage = Image.FromFile(".\\Imagenes\\" + valor.ToString() + ".jpg");
                    btnDadoSel5.Visible = true;
                    categorias[valor] += valor;
                    CalcularPuntajes();
                    break;
            }
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        void BkgImageDados()
        {
            btnDado1.BackgroundImage = Image.FromFile(".\\Imagenes\\4.jpg");
            btnDado2.BackgroundImage = Image.FromFile(".\\Imagenes\\6.jpg");
            btnDado3.BackgroundImage = Image.FromFile(".\\Imagenes\\2.jpg");
            btnDado4.BackgroundImage = Image.FromFile(".\\Imagenes\\5.jpg");
            btnDado5.BackgroundImage = Image.FromFile(".\\Imagenes\\3.jpg");
        }

        void DadosOcultar()
        {
            btnDado1.Visible = false;
            btnDado2.Visible = false;
            btnDado3.Visible = false;
            btnDado4.Visible = false;
            btnDado5.Visible = false;
        }

        void DadosSeleccionadosOcultar()
        {
            btnDadoSel1.Visible = false;
            btnDadoSel2.Visible = false;
            btnDadoSel3.Visible = false;
            btnDadoSel4.Visible = false;
            btnDadoSel5.Visible = false;
        }

        void DadosMostrarYActivar()
        {
            btnDado1.Visible = true;
            btnDado2.Visible = true;
            btnDado3.Visible = true;
            btnDado4.Visible = true;
            btnDado5.Visible = true;
            btnDado1.Enabled = true;
            btnDado2.Enabled = true;
            btnDado3.Enabled = true;
            btnDado4.Enabled = true;
            btnDado5.Enabled = true;
        }

        void DadoOcultarYDesactivar(Button b)
        {
            b.Visible = false;
            b.Enabled = false;
        }

        void DadoMostrarYActivar(Button b)
        {
            b.Visible = true;
            b.Enabled = true;
        }
        void DadoSeleccionadoMostrar(Button b)
        {
            b.Visible = true;
        }

        void CalcularPuntajes()
        {
            if (dadosElegidos == 5 || tiros == 0)
            {
                for (int i = 0; i < categorias.Length; i++)
                {
                    switch (i)
                    {
                        case 1:
                            txtUno.Text = categorias[i].ToString();
                            break;
                        case 2:
                            txtDos.Text = categorias[i].ToString();
                            break;
                        case 3:
                            txtTres.Text = categorias[i].ToString();
                            break;
                        case 4:
                            txtCuatro.Text = categorias[i].ToString();
                            break;
                        case 5:
                            txtCinco.Text = categorias[i].ToString();
                            break;
                        case 6:
                            txtSeis.Text = categorias[i].ToString();
                            break;
                    }
                }

                Array.Sort(obtenerEscalera);
                int igualdad = 0;
                for(int i = 0; i < obtenerEscalera.Length; i++)
                {
                    if(obtenerEscalera[i] == escalera[i])
                    {
                        igualdad++;
                    }
                }
                if(igualdad == 5)
                {
                    txtEscalera.Text = gestorPuntaje.Escalera(1).ToString();
                }
                else
                {
                    txtEscalera.Text = "0";
                }
            }
        }
    }
}
