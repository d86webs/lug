﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Jugador
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Jugador j)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@nombre", j.Nombre));
            acceso.Escribir("sp_jugador_insertar", parametros);

            acceso.Cerrar();
        }

        public void Modificar(BE.Jugador j)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", j.Id));
            parametros.Add(acceso.CrearParametro("@nombre", j.Nombre));
            acceso.Escribir("sp_jugador_modificar", parametros);

            acceso.Cerrar();
        }

        public void Borrar(BE.Jugador j)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", j.Id));
            acceso.Escribir("sp_jugador_borrar", parametros);

            acceso.Cerrar();
        }

        public List<BE.Jugador> Listar()
        {
            List<BE.Jugador> jugadores = new List<BE.Jugador>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_jugador_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Jugador j = new BE.Jugador();
                j.Id = int.Parse(registro[0].ToString());
                j.Nombre = registro[1].ToString();
                jugadores.Add(j);
            }

            acceso.Cerrar();

            return jugadores;
        }

        public int ObtenerIdJugador()
        {
            int jugador = 0;

            acceso.Abrir();

            DataTable idJugador = new DataTable();

            idJugador = acceso.Leer("sp_jugador_obtener_id");

            foreach (DataRow registro in idJugador.Rows)
            {
                jugador = int.Parse(registro[0].ToString());
            }

            acceso.Cerrar();

            return jugador;
        }
    }
}
