﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Partida
    {
        Acceso acceso = new Acceso();
        public DataTable ListarPartida()
        {
            DataTable partida = new DataTable();

            acceso.Abrir();

            partida = acceso.Leer("sp_partidas_listar");

            acceso.Cerrar();

            return partida;
        }

        public int ObtenerIdPartida()
        {
            int partida = 0;

            acceso.Abrir();

            DataTable idPartida = new DataTable();

            idPartida = acceso.Leer("sp_partidas_obtener");

            foreach (DataRow registro in idPartida.Rows)
            {
                partida = int.Parse(registro[0].ToString());
            }

            acceso.Cerrar();

            return partida;
        }

        public void InsertarPartida(int id_jugador1, int id_jugador2)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_jugador_1", id_jugador1));
            parametros.Add(acceso.CrearParametro("@id_jugador_2", id_jugador2));

            acceso.Escribir("sp_partidas_insertar", parametros);

            acceso.Cerrar();
        }
    }
}
