﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Empleado
    {
        private int legajo;

        public int Legajo
        {
            get { return legajo; }
            set { legajo = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string apellido;

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        private string cuil;

        public string CUIL
        {
            get { return cuil; }
            set { cuil = value; }
        }

        private DateTime fec_alta;

        public DateTime Fec_alta
        {
            get { return fec_alta; }
            set { fec_alta = value; }
        }

        public override string ToString()
        {
            return Apellido + ", " + Nombre;
        }
    }
}
