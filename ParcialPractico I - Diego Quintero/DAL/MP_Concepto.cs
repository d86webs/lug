﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class MP_Concepto
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Concepto c)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@nombre", c.Nombre));
            parametros.Add(acceso.CrearParametro("@porcentaje", c.Porcentaje));

            acceso.Escribir("sp_conceptos_insertar", parametros);

            acceso.Cerrar();
        }

        public void Modificar(BE.Concepto c)
        {
            acceso.Abrir();


            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", c.Id));
            parametros.Add(acceso.CrearParametro("@nombre", c.Nombre));
            parametros.Add(acceso.CrearParametro("@porcentaje", c.Porcentaje));

            acceso.Escribir("sp_conceptos_modificar", parametros);

            acceso.Cerrar();
        }

        public void Eliminar(BE.Concepto c)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", c.Id));
            acceso.Escribir("sp_conceptos_eliminar", parametros);

            acceso.Cerrar();
        }

        public List<BE.Concepto> Listar()
        {
            List<BE.Concepto> conceptos = new List<BE.Concepto>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_conceptos_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Concepto c = new BE.Concepto();
                c.Id = int.Parse(registro[0].ToString());
                c.Nombre = registro[1].ToString();
                c.Porcentaje = decimal.Parse(registro[2].ToString());
                conceptos.Add(c);
            }

            acceso.Cerrar();

            return conceptos;
        }
    }
}
