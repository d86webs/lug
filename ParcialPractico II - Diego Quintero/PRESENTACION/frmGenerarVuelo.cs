﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmGenerarVuelo : Form
    {
        BLL.Destino gestorDestino = new BLL.Destino();
        BLL.Vuelo gestorVuelo = new BLL.Vuelo();
        BLL.Escala gestorEsacala = new BLL.Escala();
        public frmGenerarVuelo()
        {
            InitializeComponent();
        }

        private void frmGenerarVuelo_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            Enlazar();
        }

        void Enlazar()
        {
            dgDestinos.DataSource = null;
            dgVuelos.DataSource = null;
            dgDestinos.DataSource = gestorDestino.ListarDestinos();
            dgVuelos.DataSource = gestorVuelo.ListarVuelo();
            dgDestinos.Columns[0].Visible = false;
            dgVuelos.Columns[0].Visible = false;
            dgDestinos.ClearSelection();
            dgVuelos.ClearSelection();
        }

        void LimpiarTextBox()
        {
            txtCapacidad.Limpiar();
            dgVuelos.ClearSelection();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // GENERAR VUELO
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtCapacidad.DevuelvoValor()))
            {
                foreach (Control c in this.Controls)
                {
                    if (c is miControl)
                    {
                        ((miControl)c).Validar();
                    }
                }

                MessageBox.Show("POR FAVOR COMPLETE LOS CAMPOS RESALTADOS");
            }
            else
            {
                if(!rdbNacional.Checked && !rdbInternacional.Checked)
                {
                    MessageBox.Show("DEBE ELEGIR TIPO DE VUELO");
                }
                else
                {
                    BE.Vuelo v = new BE.Vuelo();
                    BE.Destino destino = (BE.Destino)dgDestinos.SelectedRows[0].DataBoundItem;
                    v.Destino.Id = destino.Id;
                    v.Partida = dtPartida.Value;
                    v.Llegada = dtLlegada.Value;
                    v.Capacidad = int.Parse(txtCapacidad.DevuelvoValor());
                    v.Ocupados = 0;
                    if (rdbNacional.Checked) { v.Tipo = "Nacional"; }
                    if (rdbInternacional.Checked) { v.Tipo = "Internacional"; }

                    gestorVuelo.Insertar(v);
                    Enlazar();
                    LimpiarTextBox();
                }
            }
        }

        // CANCELAR VUELO
        private void btnCancelarVuelo_Click(object sender, EventArgs e)
        {
            if(dgVuelos.SelectedRows.Count > 0)
            {
                BE.Vuelo v = (BE.Vuelo)dgVuelos.SelectedRows[0].DataBoundItem;
                gestorVuelo.Eliminar(v);
                Enlazar();
                LimpiarTextBox();
                MessageBox.Show("VUELO CANCELADO");
            }
            else
            {
                MessageBox.Show("DEBE ELEGIR UN VUELO");
            }
        }

        private void frmGenerarVuelo_Activated(object sender, EventArgs e)
        {
            Enlazar();
        }

        private void dgDestinos_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnAgregarEscala_Click(object sender, EventArgs e)
        {            
        }

        private void dgVuelos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Vuelo vuelo = (BE.Vuelo)dgVuelos.SelectedRows[0].DataBoundItem;
            frmEscalas frmEscalas = new frmEscalas();
            frmEscalas.EnviarVuelo(vuelo);
            frmEscalas.ShowDialog();
        }

        private void dgVuelos_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dgVuelos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
