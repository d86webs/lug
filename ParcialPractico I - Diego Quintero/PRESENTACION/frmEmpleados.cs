﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmEmpleados : Form
    {
        BLL.Empleado gestor = new BLL.Empleado();
        public frmEmpleados()
        {
            InitializeComponent();
        }

        private void frmEmpleados_Load(object sender, EventArgs e)
        {
            txtNombre.Enabled = false;
            txtApellido.Enabled = false;
            txtCuil.Enabled = false;
            dtFecAlta.Enabled = false;
            Enlazar();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtNombre.Text) || string.IsNullOrWhiteSpace(txtApellido.Text) || string.IsNullOrWhiteSpace(txtCuil.Text))
            {
                MessageBox.Show("Falta completar uno o mas campos!");
            }
            else
            {
                BE.Empleado empleado = new BE.Empleado();
                empleado.Nombre = txtNombre.Text;
                empleado.Apellido = txtApellido.Text;
                empleado.CUIL = txtCuil.Text;
                empleado.Fec_alta = dtFecAlta.Value;

                gestor.Insertar(empleado);

                Enlazar();
            }
        }

        void Enlazar()
        {
            dgEmpleados.DataSource = null;
            dgEmpleados.DataSource = gestor.ListarEmpleados();
        }

        private void dgEmpleados_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNombre.Enabled = true;
            txtApellido.Enabled = true;
            txtCuil.Enabled = true;
            dtFecAlta.Enabled = true;

            txtLegajo.Text = dgEmpleados.Rows[e.RowIndex].Cells[0].Value.ToString();
            txtNombre.Text = dgEmpleados.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApellido.Text = dgEmpleados.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtCuil.Text = dgEmpleados.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtFecAlta.Value = DateTime.Parse(dgEmpleados.Rows[e.RowIndex].Cells[4].Value.ToString());
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNombre.Text) || string.IsNullOrWhiteSpace(txtApellido.Text) || string.IsNullOrWhiteSpace(txtCuil.Text))
            {
                MessageBox.Show("Falta completar uno o mas campos!");
            }
            else
            {
                BE.Empleado empleado = new BE.Empleado();
                empleado.Legajo = int.Parse(txtLegajo.Text);
                empleado.Nombre = txtNombre.Text;
                empleado.Apellido = txtApellido.Text;
                empleado.CUIL = txtCuil.Text;
                empleado.Fec_alta = dtFecAlta.Value;

                gestor.Insertar(empleado);
                Enlazar();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtLegajo.Clear();
            txtNombre.Enabled = true;
            txtNombre.Clear();
            txtApellido.Enabled = true;
            txtApellido.Clear();
            txtCuil.Enabled = true;
            txtCuil.Clear();
            dtFecAlta.Enabled = true;
            dtFecAlta.Value = DateTime.Now;
        }

        private void dgEmpleados_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtLegajo.Text))
            {
                MessageBox.Show("Debe elegir un empleado!");
            }
            else
            {
                BE.Empleado empleado = new BE.Empleado();
                empleado.Legajo = int.Parse(txtLegajo.Text);

                gestor.Eliminar(empleado);

                Enlazar();
            }
        }
    }
}
