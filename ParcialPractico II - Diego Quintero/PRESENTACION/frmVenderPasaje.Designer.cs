﻿namespace PRESENTACION
{
    partial class frmVenderPasaje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgPasajeros = new System.Windows.Forms.DataGridView();
            this.dgVuelos = new System.Windows.Forms.DataGridView();
            this.dgPasajes = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnVenderPasaje = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgPasajeros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVuelos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPasajes)).BeginInit();
            this.SuspendLayout();
            // 
            // dgPasajeros
            // 
            this.dgPasajeros.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgPasajeros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPasajeros.Location = new System.Drawing.Point(45, 37);
            this.dgPasajeros.MultiSelect = false;
            this.dgPasajeros.Name = "dgPasajeros";
            this.dgPasajeros.ReadOnly = true;
            this.dgPasajeros.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPasajeros.Size = new System.Drawing.Size(345, 366);
            this.dgPasajeros.TabIndex = 0;
            // 
            // dgVuelos
            // 
            this.dgVuelos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgVuelos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVuelos.Location = new System.Drawing.Point(396, 37);
            this.dgVuelos.MultiSelect = false;
            this.dgVuelos.Name = "dgVuelos";
            this.dgVuelos.ReadOnly = true;
            this.dgVuelos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVuelos.Size = new System.Drawing.Size(647, 366);
            this.dgVuelos.TabIndex = 1;
            this.dgVuelos.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVuelos_CellContentDoubleClick);
            // 
            // dgPasajes
            // 
            this.dgPasajes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgPasajes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPasajes.Location = new System.Drawing.Point(45, 475);
            this.dgPasajes.MultiSelect = false;
            this.dgPasajes.Name = "dgPasajes";
            this.dgPasajes.ReadOnly = true;
            this.dgPasajes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPasajes.Size = new System.Drawing.Size(998, 334);
            this.dgPasajes.TabIndex = 2;
            this.dgPasajes.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPasajes_CellContentDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(189, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "PASAJEROS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(652, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "VUELOS DISPONIBLES";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(477, 459);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "PASAJES VENDIDOS";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(968, 815);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "SALIR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnVenderPasaje
            // 
            this.btnVenderPasaje.Location = new System.Drawing.Point(472, 409);
            this.btnVenderPasaje.Name = "btnVenderPasaje";
            this.btnVenderPasaje.Size = new System.Drawing.Size(118, 32);
            this.btnVenderPasaje.TabIndex = 7;
            this.btnVenderPasaje.Text = "VENDER PASAJE";
            this.btnVenderPasaje.UseVisualStyleBackColor = true;
            this.btnVenderPasaje.Click += new System.EventHandler(this.btnVenderPasaje_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(811, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(180, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "(Doble Click para Ver Escalas)";
            // 
            // frmVenderPasaje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 847);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnVenderPasaje);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgPasajes);
            this.Controls.Add(this.dgVuelos);
            this.Controls.Add(this.dgPasajeros);
            this.Name = "frmVenderPasaje";
            this.Text = "VENDER PASAJE";
            this.Load += new System.EventHandler(this.frmVender_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgPasajeros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgVuelos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgPasajes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgPasajeros;
        private System.Windows.Forms.DataGridView dgVuelos;
        private System.Windows.Forms.DataGridView dgPasajes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnVenderPasaje;
        private System.Windows.Forms.Label label4;
    }
}