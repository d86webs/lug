﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    internal class Acceso
    {
        private SqlConnection conexion;

        public void Abrir()
        {
            conexion = new SqlConnection(@"Data Source = .\SQLEXPRESS; Initial Catalog = dbSueldos; Integrated Security = SSPI");
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CrearComando(string SQL, List<IDbDataParameter> parametros = null, CommandType tipo = CommandType.StoredProcedure)
        {
            SqlCommand comando = new SqlCommand(SQL, conexion);
            comando.CommandType = tipo;

            if(parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            return comando;
        }

        public DataTable Leer(string SQL, List<IDbDataParameter> parametros = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(SQL, parametros);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);

            return tabla;
        }

        public int Escribir(string SQL, List<IDbDataParameter> parametros = null)
        {
            SqlCommand comando = CrearComando(SQL, parametros);
            int resultado;

            try
            {
                resultado = comando.ExecuteNonQuery();
            }
            catch
            {
                resultado = -1;
            }

            return resultado;
        }

        public IDbDataParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Int32;
            return p;
        }

        public IDbDataParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.String;
            return p;
        }

        public IDbDataParameter CrearParametro(string nombre, decimal valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Decimal;
            return p;
        }

        public IDbDataParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Double;
            return p;
        }

        public IDbDataParameter CrearParametro(string nombre, bool valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Boolean;
            return p;
        }

        public IDbDataParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter p = new SqlParameter(nombre, valor);
            p.DbType = DbType.Date;
            return p;
        }
    }
}
