﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Vuelo
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private Destino destino = new Destino();

        public Destino Destino
        {
            get { return destino; }
            set { destino = value; }
        }


        private DateTime partida;

        public DateTime Partida
        {
            get { return partida; }
            set { partida = value; }
        }

        private DateTime llegada;

        public DateTime Llegada
        {
            get { return llegada; }
            set { llegada = value; }
        }

        private int capacidad;

        public int Capacidad
        {
            get { return capacidad; }
            set { capacidad = value; }
        }

        private int ocupados;

        public int Ocupados
        {
            get { return ocupados; }
            set { ocupados = value; }
        }

        private string tipo;

        public string Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        public override string ToString()
        {
            return "DESTINO: " + Destino.Nombre + ", " + Destino.Pais + " - PARTIDA: " + Partida.ToString() + " - LLEGADA: " + Llegada.ToString(); 
        }
    }
}
