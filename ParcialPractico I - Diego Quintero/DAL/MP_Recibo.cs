﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class MP_Recibo
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Recibo r)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@legajo", r.Legajo));
            parametros.Add(acceso.CrearParametro("@mes", r.Mes));
            parametros.Add(acceso.CrearParametro("@ano", r.Ano));
            parametros.Add(acceso.CrearParametro("@bruto", r.Bruto));
            parametros.Add(acceso.CrearParametro("@neto", r.Neto));
            parametros.Add(acceso.CrearParametro("@descuento", r.Descuento));

            acceso.Escribir("sp_recibo_insertar", parametros);

            acceso.Cerrar();
        }

        // RECIBOS LISTAR
        public List<BE.Recibo> Listar()
        {
            List<BE.Recibo> recibos = new List<BE.Recibo>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_recibo_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Recibo r = new BE.Recibo();
                r.Id = int.Parse(registro[0].ToString());
                r.Legajo = int.Parse(registro[1].ToString());
                r.Mes = int.Parse(registro[2].ToString());
                r.Ano = int.Parse(registro[3].ToString());
                r.Bruto = decimal.Parse(registro[4].ToString());
                r.Descuento = decimal.Parse(registro[5].ToString());
                r.Neto = decimal.Parse(registro[6].ToString());
                recibos.Add(r);
            }

            acceso.Cerrar();

            return recibos;
        }
    }
}
