﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BLL
{
    public class Destino
    {
        DAL.MP_Destino mp = new DAL.MP_Destino();

        public void Insertar(BE.Destino d)
        {
            if (d.Id == 0)
            {
                mp.Insertar(d);
            }
            else
            {
                mp.Actualizar(d);
            }
        }

        public void Eliminar(BE.Destino d)
        {
            mp.Eliminar(d);
        }

        public List<BE.Destino> ListarDestinos()
        {
            return mp.Listar();
        }
    }
}
