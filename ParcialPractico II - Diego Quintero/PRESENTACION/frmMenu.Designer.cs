﻿namespace PRESENTACION
{
    partial class frmMenu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aBMsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dESTINOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pASAJEROSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vUELOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gENERAAARToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vENDERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sALIRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cONSULTASToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aBMsToolStripMenuItem,
            this.vUELOSToolStripMenuItem,
            this.cONSULTASToolStripMenuItem,
            this.sALIRToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aBMsToolStripMenuItem
            // 
            this.aBMsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dESTINOSToolStripMenuItem,
            this.pASAJEROSToolStripMenuItem});
            this.aBMsToolStripMenuItem.Name = "aBMsToolStripMenuItem";
            this.aBMsToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.aBMsToolStripMenuItem.Text = "ABM\'s";
            // 
            // dESTINOSToolStripMenuItem
            // 
            this.dESTINOSToolStripMenuItem.Name = "dESTINOSToolStripMenuItem";
            this.dESTINOSToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.dESTINOSToolStripMenuItem.Text = "DESTINOS";
            this.dESTINOSToolStripMenuItem.Click += new System.EventHandler(this.dESTINOSToolStripMenuItem_Click);
            // 
            // pASAJEROSToolStripMenuItem
            // 
            this.pASAJEROSToolStripMenuItem.Name = "pASAJEROSToolStripMenuItem";
            this.pASAJEROSToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.pASAJEROSToolStripMenuItem.Text = "PASAJEROS";
            this.pASAJEROSToolStripMenuItem.Click += new System.EventHandler(this.pASAJEROSToolStripMenuItem_Click);
            // 
            // vUELOSToolStripMenuItem
            // 
            this.vUELOSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gENERAAARToolStripMenuItem,
            this.vENDERToolStripMenuItem});
            this.vUELOSToolStripMenuItem.Name = "vUELOSToolStripMenuItem";
            this.vUELOSToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.vUELOSToolStripMenuItem.Text = "VUELOS";
            // 
            // gENERAAARToolStripMenuItem
            // 
            this.gENERAAARToolStripMenuItem.Name = "gENERAAARToolStripMenuItem";
            this.gENERAAARToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.gENERAAARToolStripMenuItem.Text = "GENERAR";
            this.gENERAAARToolStripMenuItem.Click += new System.EventHandler(this.gENERAAARToolStripMenuItem_Click);
            // 
            // vENDERToolStripMenuItem
            // 
            this.vENDERToolStripMenuItem.Name = "vENDERToolStripMenuItem";
            this.vENDERToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.vENDERToolStripMenuItem.Text = "VENDER";
            this.vENDERToolStripMenuItem.Click += new System.EventHandler(this.vENDERToolStripMenuItem_Click);
            // 
            // sALIRToolStripMenuItem
            // 
            this.sALIRToolStripMenuItem.Name = "sALIRToolStripMenuItem";
            this.sALIRToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.sALIRToolStripMenuItem.Text = "SALIR";
            this.sALIRToolStripMenuItem.Click += new System.EventHandler(this.sALIRToolStripMenuItem_Click);
            // 
            // cONSULTASToolStripMenuItem
            // 
            this.cONSULTASToolStripMenuItem.Name = "cONSULTASToolStripMenuItem";
            this.cONSULTASToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.cONSULTASToolStripMenuItem.Text = "CONSULTAS";
            this.cONSULTASToolStripMenuItem.Click += new System.EventHandler(this.cONSULTASToolStripMenuItem_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenu";
            this.Text = "MENU";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Menu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aBMsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sALIRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dESTINOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pASAJEROSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vUELOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gENERAAARToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vENDERToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cONSULTASToolStripMenuItem;
    }
}

