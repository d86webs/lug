USE [AEROPUERTO]
GO
/****** Object:  Table [dbo].[tb_destinos]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_destinos](
	[id_destino] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[pais] [varchar](50) NOT NULL,
	[localizacion] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tb_destinos] PRIMARY KEY CLUSTERED 
(
	[id_destino] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_escalas]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_escalas](
	[id_escala] [int] NOT NULL,
	[id_vuelo] [int] NOT NULL,
	[id_destino] [int] NOT NULL,
 CONSTRAINT [PK_tb_escalas] PRIMARY KEY CLUSTERED 
(
	[id_escala] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_pasajeros]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_pasajeros](
	[id_pasajero] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[dni] [int] NOT NULL,
 CONSTRAINT [PK_tb_pasajeros] PRIMARY KEY CLUSTERED 
(
	[id_pasajero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_pasajes]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_pasajes](
	[id_pasaje] [int] NOT NULL,
	[id_pasajero] [int] NOT NULL,
	[id_vuelo] [int] NOT NULL,
 CONSTRAINT [PK_tb_vuelos_ventidos] PRIMARY KEY CLUSTERED 
(
	[id_pasaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_vuelos]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_vuelos](
	[id_vuelo] [int] NOT NULL,
	[id_destino] [int] NOT NULL,
	[partida] [datetime] NOT NULL,
	[llegada] [datetime] NOT NULL,
	[capacidad] [int] NOT NULL,
	[ocupados] [int] NOT NULL,
	[tipo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tb_vuelos] PRIMARY KEY CLUSTERED 
(
	[id_vuelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_destinos_actualizar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_destinos_actualizar]
	@id int,
	@nombre varchar(50),
	@pais varchar(50),
	@localizacion varchar(max)
as
begin

	update tb_destinos set nombre = @nombre, pais = @pais, localizacion = @localizacion where id_destino = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_destinos_eliminar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_destinos_eliminar]
	@id int
as
begin

	delete from tb_destinos where id_destino = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_destinos_insertar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_destinos_insertar]
	@nombre varchar(50),
	@pais varchar(50),
	@localizacion varchar(max)
as
begin
	declare @id int
	set @id = (select isnull(max(id_destino), 0) + 1 from tb_destinos)

	insert into tb_destinos values (@id, @nombre, @pais, @localizacion)

end
GO
/****** Object:  StoredProcedure [dbo].[sp_destinos_listar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_destinos_listar]
as
begin
	select id_destino, nombre, pais, localizacion from tb_destinos order by id_destino
end
GO
/****** Object:  StoredProcedure [dbo].[sp_escalas_actualizar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_escalas_actualizar]
	@id int,
	@id_vuelo int,
	@id_destino int
as
begin

	update tb_escalas set id_vuelo = @id_vuelo, id_destino = @id_destino where id_escala = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_escalas_eliminar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_escalas_eliminar]
	@id int
as
begin

	delete from tb_escalas where id_escala = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_escalas_insertar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_escalas_insertar]
	@id_vuelo int,
	@id_destino int
as
begin
	declare @id int
	set @id = (select isnull(max(id_escala), 0) + 1 from tb_escalas)

	insert into tb_escalas values (@id, @id_vuelo, @id_destino)

end
GO
/****** Object:  StoredProcedure [dbo].[sp_escalas_listar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec sp_escalas_listar 2
CREATE proc [dbo].[sp_escalas_listar]
	@id_vuelo int
as
begin

	select a.id_escala, c.nombre Escala, c.pais Pais
	from tb_escalas a
	left join tb_vuelos b on a.id_vuelo = b.id_vuelo
	left join tb_destinos c on c.id_destino = a.id_destino
	where a.id_vuelo = @id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_pasajeros_actualizar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_pasajeros_actualizar]
	@id int,
	@nombre varchar(50),
	@apellido varchar(50),
	@dni int
as
begin

	update tb_pasajeros set nombre = @nombre, apellido = @apellido, dni = @dni where id_pasajero = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_pasajeros_eliminar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_pasajeros_eliminar]
	@id int
as
begin

	delete from tb_pasajeros where id_pasajero = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_pasajeros_insertar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_pasajeros_insertar]
	@nombre varchar(50),
	@apellido varchar(50),
	@dni int
as
begin
	declare @id int
	set @id = (select isnull(max(id_pasajero), 0) + 1 from tb_pasajeros)

	insert into tb_pasajeros values (@id, @nombre, @apellido, @dni)

end
GO
/****** Object:  StoredProcedure [dbo].[sp_pasajeros_listar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_pasajeros_listar]
as
begin

	select id_pasajero, nombre, apellido, dni from tb_pasajeros order by id_pasajero

end
GO
/****** Object:  StoredProcedure [dbo].[sp_pasajes_insertar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_pasajes_insertar]
	@id_pasajero int,
	@id_vuelo int
as
begin
	declare @id int
	set @id = (select isnull(max(id_pasaje), 0) + 1 from tb_pasajes)

	insert into tb_pasajes values (@id, @id_pasajero, @id_vuelo)

	update tb_vuelos set ocupados = ocupados + 1 where id_vuelo = @id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_pasajes_listar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_pasajes_listar]
as
begin
	select a.id_pasaje, b.nombre Nombre, b.apellido Apellido, b.dni DNI, d.nombre Destino, d.pais Pais, c.partida Partida, c.llegada Llegada, c.tipo Tipo, a.id_vuelo
	from tb_pasajes a
	left join tb_pasajeros b on b.id_pasajero = a.id_pasajero
	left join tb_vuelos c on c.id_vuelo =  a.id_vuelo
	left join tb_destinos d on d.id_destino = c.id_destino
end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_actualizar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_actualizar]
	@id int,
	@id_destino int,
	@partida datetime,
	@llegada datetime,
	@capacidad int,
	@ocupados int,
	@tipo varchar(50)
as
begin

	update tb_vuelos set id_destino = @id_destino, partida = @partida, llegada = @llegada, capacidad = @capacidad, ocupados = @ocupados, tipo = @tipo where id_vuelo = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_eliminar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_vuelos_eliminar]
	@id int
as
begin

	delete from tb_vuelos where id_vuelo = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_insertar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_insertar]
	@id_destino int,
	@partida datetime,
	@llegada datetime,
	@capacidad int,
	@ocupados int,
	@tipo varchar(50)
as
begin
	declare @id int
	set @id = (select isnull(max(id_vuelo), 0) + 1 from tb_vuelos)

	insert into tb_vuelos values (@id, @id_destino, @partida, @llegada, @capacidad, @ocupados, @tipo)

end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_listar]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_listar]
as
begin
	select a.id_vuelo, b.nombre, b.pais, a.partida, a.llegada, a.capacidad, a.ocupados, a.tipo
	from tb_vuelos a
	left join tb_destinos b on b.id_destino = a.id_destino
	order by a.id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_listar_ocupacion_menor_a_40]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_listar_ocupacion_menor_a_40]
as
begin 
	select	a.id_vuelo, 
			b.nombre Nombre, 
			b.pais Pais, 
			a.partida Partida, 
			a.llegada Llegada, 
			a.capacidad Capacidad, 
			a.ocupados Ocupados, 
			round((cast(a.ocupados as float) * 100.00) / cast(a.capacidad as float), 2) [Disponible %], 
			a.tipo Tipo, 
			b.id_destino
	from tb_vuelos a
	left join tb_destinos b on b.id_destino = a.id_destino
	where (round((cast(a.ocupados as float) * 100.00) / cast(a.capacidad as float), 2)) < 40.00 and cast(a.llegada as date) < cast(getdate() as date) 
	order by a.id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_listarPorDestino]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec sp_vuelos_listarPorDestino 2
CREATE proc [dbo].[sp_vuelos_listarPorDestino]
	@id_destino int
as
begin
		select a.id_vuelo, b.nombre Nombre, b.pais Pais, a.partida Partida, a.llegada Llegada, a.capacidad Capacidad, a.ocupados Ocupados, round((cast(a.ocupados as float) * 100.00) / cast(a.capacidad as float), 2) [% Ocupado], a.tipo Tipo, b.id_destino
		from tb_vuelos a
		left join tb_destinos b on b.id_destino = a.id_destino
		where b.id_destino = @id_destino
		order by a.id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_listarPorFechaLlegada]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_listarPorFechaLlegada]
	@fecha date
as
begin
		select a.id_vuelo, b.nombre Nombre, b.pais Pais, a.partida Partida, a.llegada Llegada, a.capacidad Capacidad, a.ocupados Ocupados, round((cast(a.ocupados as float) * 100.00) / cast(a.capacidad as float), 2) [% Ocupado], a.tipo Tipo, b.id_destino
		from tb_vuelos a
		left join tb_destinos b on b.id_destino = a.id_destino
		where cast(a.llegada as date) = @fecha
		order by a.id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_listarPorFechaPartida]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_listarPorFechaPartida]
	@fecha date
as
begin
		select a.id_vuelo, b.nombre Nombre, b.pais Pais, a.partida Partida, a.llegada Llegada, a.capacidad Capacidad, a.ocupados Ocupados, round((cast(a.ocupados as float) * 100.00) / cast(a.capacidad as float), 2) [% Ocupado], a.tipo Tipo, b.id_destino
		from tb_vuelos a
		left join tb_destinos b on b.id_destino = a.id_destino
		where cast(a.partida as date) = @fecha
		order by a.id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_listarPorTipoInternacional]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_listarPorTipoInternacional]
as
begin
		select a.id_vuelo, b.nombre Nombre, b.pais Pais, a.partida Partida, a.llegada Llegada, a.capacidad Capacidad, a.ocupados Ocupados, round((cast(a.ocupados as float) * 100.00) / cast(a.capacidad as float), 2) [% Ocupado], a.tipo Tipo, b.id_destino
		from tb_vuelos a
		left join tb_destinos b on b.id_destino = a.id_destino
		where a.tipo = 'Internacional'
		order by a.id_vuelo
end
GO
/****** Object:  StoredProcedure [dbo].[sp_vuelos_listarPorTipoNacional]    Script Date: 17/11/2020 17:09:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_vuelos_listarPorTipoNacional]
as
begin
		select a.id_vuelo, b.nombre Nombre, b.pais Pais, a.partida Partida, a.llegada Llegada, a.capacidad Capacidad, a.ocupados Ocupados, round((cast(a.ocupados as float) * 100.00) / cast(a.capacidad as float), 2) [% Ocupado], a.tipo Tipo, b.id_destino
		from tb_vuelos a
		left join tb_destinos b on b.id_destino = a.id_destino
		where a.tipo = 'Nacional'
		order by a.id_vuelo
end
GO
