﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class MP_Categorias
    {
        Acceso acceso = new Acceso();
        public int Escalera(int tipo)
        {
            int puntaje = 0;

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_puntaje", 6));
            parametros.Add(acceso.CrearParametro("@tipo", tipo));

            string SQL = string.Format("sp_puntajes_obtener");

            DataTable tabla = acceso.Leer(SQL, parametros);

            foreach (DataRow registro in tabla.Rows)
            {
                puntaje = int.Parse(registro[0].ToString());
            }

            acceso.Cerrar();

            return puntaje;
        }
    }
}
