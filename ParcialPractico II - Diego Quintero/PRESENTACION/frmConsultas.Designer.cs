﻿namespace PRESENTACION
{
    partial class frmConsultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgDestinos = new System.Windows.Forms.DataGridView();
            this.dgResultados = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnListarPorDestino = new System.Windows.Forms.Button();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btnListarPorFechaPartida = new System.Windows.Forms.Button();
            this.btnListarPorFechaLlegada = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnListarVuelosInternacionales = new System.Windows.Forms.Button();
            this.btnListarVuelosNacionales = new System.Windows.Forms.Button();
            this.btnVuelosOcupacionMenorA40 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgDestinos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgResultados)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgDestinos
            // 
            this.dgDestinos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgDestinos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDestinos.Location = new System.Drawing.Point(35, 38);
            this.dgDestinos.MultiSelect = false;
            this.dgDestinos.Name = "dgDestinos";
            this.dgDestinos.ReadOnly = true;
            this.dgDestinos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDestinos.Size = new System.Drawing.Size(309, 262);
            this.dgDestinos.TabIndex = 0;
            this.dgDestinos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDestinos_CellClick);
            // 
            // dgResultados
            // 
            this.dgResultados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgResultados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgResultados.Location = new System.Drawing.Point(12, 335);
            this.dgResultados.MultiSelect = false;
            this.dgResultados.Name = "dgResultados";
            this.dgResultados.ReadOnly = true;
            this.dgResultados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgResultados.Size = new System.Drawing.Size(1082, 459);
            this.dgResultados.TabIndex = 1;
            this.dgResultados.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgResultados_CellContentDoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(165, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "DESTINOS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(516, 319);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "RESULTADOS";
            // 
            // btnListarPorDestino
            // 
            this.btnListarPorDestino.Location = new System.Drawing.Point(350, 38);
            this.btnListarPorDestino.Name = "btnListarPorDestino";
            this.btnListarPorDestino.Size = new System.Drawing.Size(153, 36);
            this.btnListarPorDestino.TabIndex = 4;
            this.btnListarPorDestino.Text = "LISTAR POR DESTINO";
            this.btnListarPorDestino.UseVisualStyleBackColor = true;
            this.btnListarPorDestino.Click += new System.EventHandler(this.btnListarPorDestino_Click);
            // 
            // dtpFecha
            // 
            this.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFecha.Location = new System.Drawing.Point(534, 39);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(102, 20);
            this.dtpFecha.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(531, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "FECHA";
            // 
            // btnListarPorFechaPartida
            // 
            this.btnListarPorFechaPartida.Location = new System.Drawing.Point(534, 65);
            this.btnListarPorFechaPartida.Name = "btnListarPorFechaPartida";
            this.btnListarPorFechaPartida.Size = new System.Drawing.Size(153, 36);
            this.btnListarPorFechaPartida.TabIndex = 9;
            this.btnListarPorFechaPartida.Text = "LISTAR POR FECHA DE PARTIDA";
            this.btnListarPorFechaPartida.UseVisualStyleBackColor = true;
            this.btnListarPorFechaPartida.Click += new System.EventHandler(this.btnListarPorFechaPartida_Click);
            // 
            // btnListarPorFechaLlegada
            // 
            this.btnListarPorFechaLlegada.Location = new System.Drawing.Point(534, 107);
            this.btnListarPorFechaLlegada.Name = "btnListarPorFechaLlegada";
            this.btnListarPorFechaLlegada.Size = new System.Drawing.Size(153, 36);
            this.btnListarPorFechaLlegada.TabIndex = 10;
            this.btnListarPorFechaLlegada.Text = "LISTAR POR FECHA DE LLEGADA";
            this.btnListarPorFechaLlegada.UseVisualStyleBackColor = true;
            this.btnListarPorFechaLlegada.Click += new System.EventHandler(this.btnListarPorFechaLlegada_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnListarVuelosInternacionales);
            this.groupBox1.Controls.Add(this.btnListarVuelosNacionales);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(726, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(324, 120);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TIPO DE VUELO";
            // 
            // btnListarVuelosInternacionales
            // 
            this.btnListarVuelosInternacionales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarVuelosInternacionales.Location = new System.Drawing.Point(165, 40);
            this.btnListarVuelosInternacionales.Name = "btnListarVuelosInternacionales";
            this.btnListarVuelosInternacionales.Size = new System.Drawing.Size(153, 36);
            this.btnListarVuelosInternacionales.TabIndex = 13;
            this.btnListarVuelosInternacionales.Text = "LISTAR VUELOS INTERNACIONALES";
            this.btnListarVuelosInternacionales.UseVisualStyleBackColor = true;
            this.btnListarVuelosInternacionales.Click += new System.EventHandler(this.btnListarVuelosInternacionales_Click);
            // 
            // btnListarVuelosNacionales
            // 
            this.btnListarVuelosNacionales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListarVuelosNacionales.Location = new System.Drawing.Point(6, 40);
            this.btnListarVuelosNacionales.Name = "btnListarVuelosNacionales";
            this.btnListarVuelosNacionales.Size = new System.Drawing.Size(153, 36);
            this.btnListarVuelosNacionales.TabIndex = 12;
            this.btnListarVuelosNacionales.Text = "LISTAR VUELOS NACIONALES";
            this.btnListarVuelosNacionales.UseVisualStyleBackColor = true;
            this.btnListarVuelosNacionales.Click += new System.EventHandler(this.btnListarVuelosNacionales_Click);
            // 
            // btnVuelosOcupacionMenorA40
            // 
            this.btnVuelosOcupacionMenorA40.Location = new System.Drawing.Point(607, 191);
            this.btnVuelosOcupacionMenorA40.Name = "btnVuelosOcupacionMenorA40";
            this.btnVuelosOcupacionMenorA40.Size = new System.Drawing.Size(153, 36);
            this.btnVuelosOcupacionMenorA40.TabIndex = 12;
            this.btnVuelosOcupacionMenorA40.Text = "LISTAR";
            this.btnVuelosOcupacionMenorA40.UseVisualStyleBackColor = true;
            this.btnVuelosOcupacionMenorA40.Click += new System.EventHandler(this.btnVuelosOcupacionMenorA40_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(531, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(335, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "VUELOS FINALIZADOS CON OCUPACION MENOR A 40%";
            // 
            // frmConsultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 836);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnVuelosOcupacionMenorA40);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnListarPorFechaLlegada);
            this.Controls.Add(this.btnListarPorFechaPartida);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.btnListarPorDestino);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgResultados);
            this.Controls.Add(this.dgDestinos);
            this.Name = "frmConsultas";
            this.Text = "CONSULTAS";
            this.Load += new System.EventHandler(this.frmConsultas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgDestinos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgResultados)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgDestinos;
        private System.Windows.Forms.DataGridView dgResultados;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnListarPorDestino;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnListarPorFechaPartida;
        private System.Windows.Forms.Button btnListarPorFechaLlegada;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnListarVuelosInternacionales;
        private System.Windows.Forms.Button btnListarVuelosNacionales;
        private System.Windows.Forms.Button btnVuelosOcupacionMenorA40;
        private System.Windows.Forms.Label label4;
    }
}