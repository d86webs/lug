﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Concepto
    {
        DAL.MP_Concepto mp = new DAL.MP_Concepto();

        public void Insertar(BE.Concepto c)
        {
            if (c.Id == 0)
            {
                mp.Insertar(c);
            }
            else
            {
                mp.Modificar(c);
            }
        }

        public void Eliminar(BE.Concepto c)
        {
            mp.Eliminar(c);
        }

        public List<BE.Concepto> ListarConceptos()
        {
            return mp.Listar();
        }
    }
}
