﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmPasajeros : Form
    {
        BLL.Pasajero gestorPasajero = new BLL.Pasajero();
        public frmPasajeros()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPasajeros_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            EnlazarPasajeros();
            LimpiarTextBox();
        }

        void EnlazarPasajeros()
        {
            dgPasajeros.DataSource = null;
            dgPasajeros.DataSource = gestorPasajero.ListarPasajeros();
            dgPasajeros.Columns[0].Visible = false;
        }

        void LimpiarTextBox()
        {
            txtNombre.Limpiar();
            txtApellido.Limpiar();
            txtDni.Limpiar();
            dgPasajeros.ClearSelection();
            txtNombre.Focus();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtNombre.DevuelvoValor()) || string.IsNullOrWhiteSpace(txtApellido.DevuelvoValor()) || string.IsNullOrWhiteSpace(txtDni.DevuelvoValor()))
            {
                foreach (Control c in this.Controls)
                {
                    if (c is miControl)
                    {
                        ((miControl)c).Validar();
                    }
                }

                MessageBox.Show("POR FAVOR COMPLETE LOS CAMPOS RESALTADOS");
            }
            else
            {
                BE.Pasajero pasajero = new BE.Pasajero();
                pasajero.Nombre = txtNombre.DevuelvoValor();
                pasajero.Apellido = txtApellido.DevuelvoValor();
                pasajero.Dni = int.Parse(txtDni.DevuelvoValor());
                gestorPasajero.Insertar(pasajero);
                EnlazarPasajeros();
                LimpiarTextBox();
            }           
        }

        private void dgPasajeros_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BE.Pasajero pasajero = new BE.Pasajero();
            pasajero.Id = int.Parse(dgPasajeros.Rows[e.RowIndex].Cells[0].Value.ToString());
            pasajero.Nombre = dgPasajeros.Rows[e.RowIndex].Cells[1].Value.ToString();
            pasajero.Apellido = dgPasajeros.Rows[e.RowIndex].Cells[2].Value.ToString();
            pasajero.Dni = int.Parse(dgPasajeros.Rows[e.RowIndex].Cells[3].Value.ToString());

            txtNombre.LlenarCampo(pasajero.Nombre);
            txtApellido.LlenarCampo(pasajero.Apellido);
            txtDni.LlenarCampo(pasajero.Dni.ToString());
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            BE.Pasajero pasajero = (BE.Pasajero)dgPasajeros.SelectedRows[0].DataBoundItem;
            pasajero.Nombre = txtNombre.DevuelvoValor();
            pasajero.Apellido = txtApellido.DevuelvoValor();
            pasajero.Dni = int.Parse(txtDni.DevuelvoValor());
            gestorPasajero.Insertar(pasajero);
            EnlazarPasajeros();
            LimpiarTextBox();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            BE.Pasajero pasajero = (BE.Pasajero)dgPasajeros.SelectedRows[0].DataBoundItem;
            gestorPasajero.Eliminar(pasajero);
            EnlazarPasajeros();
            LimpiarTextBox();
        }
    }
}
