﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmEscalas : Form
    {
        BLL.Destino gestorDestino = new BLL.Destino();
        BLL.Escala gestorEscala = new BLL.Escala();
        BE.Vuelo vuelo = new BE.Vuelo();
        public frmEscalas()
        {
            InitializeComponent();
        }

        private void frmEscalas_Load(object sender, EventArgs e)
        {
            Enlazar();
        }

        void Enlazar()
        {
            dgDestinos.DataSource = null;
            dgDestinos.DataSource = gestorDestino.ListarDestinos();
            dgDestinos.Columns[0].Visible = false;
            dgDestinos.ClearSelection();
        }

        private void dgDestinos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Destino destino = (BE.Destino)dgDestinos.SelectedRows[0].DataBoundItem;
            BE.Escala escala = new BE.Escala();
            escala.Id_vuelo = vuelo.Id;
            escala.Id_destino = destino.Id;
            gestorEscala.Insertar(escala);
            MessageBox.Show("ESCALA AGREGADA");
            this.Close();
        }

        public void EnviarVuelo(BE.Vuelo v)
        {
            vuelo = v;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
