﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Empleado
    {
        private Acceso acceso = new Acceso();
        MP_Recibo gestorRecibo = new MP_Recibo();

        public void Insertar(BE.Empleado e)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@nombre", e.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", e.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", e.CUIL));
            parametros.Add(acceso.CrearParametro("@fec_alta", e.Fec_alta));

            acceso.Escribir("sp_empleados_insertar", parametros);

            acceso.Cerrar();
        }

        public void Modificar(BE.Empleado e)
        {
            acceso.Abrir();


            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@legajo", e.Legajo));
            parametros.Add(acceso.CrearParametro("@nombre", e.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", e.Apellido));
            parametros.Add(acceso.CrearParametro("@cuil", e.CUIL));
            parametros.Add(acceso.CrearParametro("@fec_alta", e.Fec_alta));

            acceso.Escribir("sp_empleados_modificar", parametros);

            acceso.Cerrar();
        }

        public void Eliminar(BE.Empleado e)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@legajo", e.Legajo));
            acceso.Escribir("sp_empleados_eliminar", parametros);

            acceso.Cerrar();
        }

        public List<BE.Empleado> Listar()
        {
            List<BE.Empleado> empleados = new List<BE.Empleado>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_empleados_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Empleado e = new BE.Empleado();
                e.Legajo = int.Parse(registro[0].ToString());
                e.Nombre = registro[1].ToString();
                e.Apellido = registro[2].ToString();
                e.CUIL = registro[3].ToString();
                e.Fec_alta = DateTime.Parse(registro[4].ToString());
                empleados.Add(e);
            }

            acceso.Cerrar();

            return empleados;
        }

        public List<BE.Recibo> ListarRecibosPorEmpleado(BE.Empleado e)
        {
            List<BE.Recibo> recibos = new List<BE.Recibo>();

            acceso.Abrir();

            foreach (BE.Recibo r in gestorRecibo.Listar())
            {
                if(r.Legajo == e.Legajo)
                {
                    recibos.Add(r);
                }
            }

            acceso.Cerrar();

            return recibos;
        }

        public BE.Recibo ListarRecibosPorFecha(BE.Empleado e, BE.Ano a, BE.Mes m)
        {
            BE.Recibo recibo = new BE.Recibo();

            acceso.Abrir();

            foreach (BE.Recibo r in gestorRecibo.Listar())
            {
                if (r.Legajo == e.Legajo && r.Ano == a.Id && r.Mes == m.Id )
                {
                    recibo = r;
                }
            }

            acceso.Cerrar();

            return recibo;
        }
    }
}
