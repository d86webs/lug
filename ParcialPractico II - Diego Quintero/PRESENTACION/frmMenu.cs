﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void sALIRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void dESTINOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmDestinos frmDestinos = new frmDestinos();
            frmDestinos.MdiParent = this;
            frmDestinos.Show();
        }

        private void pASAJEROSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPasajeros frmPasajeros = new frmPasajeros();
            frmPasajeros.MdiParent = this;
            frmPasajeros.Show();
        }

        private void gENERAAARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGenerarVuelo frmGenerarVuelo = new frmGenerarVuelo();
            frmGenerarVuelo.MdiParent = this;
            frmGenerarVuelo.Show();
        }

        private void vENDERToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmVenderPasaje frmVenderPasaje = new frmVenderPasaje();
            frmVenderPasaje.MdiParent = this;
            frmVenderPasaje.Show();
        }

        private void cONSULTASToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultas frmConsultas = new frmConsultas();
            frmConsultas.MdiParent = this;
            frmConsultas.Show();
        }
    }
}
