﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmDestinos : Form
    {
        BLL.Destino gestorDestino = new BLL.Destino();
        public frmDestinos()
        {
            InitializeComponent();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmDestinos_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            EnlazarDestinos();
            LimpiarTextBox();
        }

        void EnlazarDestinos()
        {
            dgDestinos.DataSource = null;
            dgDestinos.DataSource = gestorDestino.ListarDestinos();
            dgDestinos.ClearSelection();
            dgDestinos.Columns[0].Visible = false;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrWhiteSpace(txtNombre.DevuelvoValor()) || string.IsNullOrWhiteSpace(txtPais.DevuelvoValor()) || string.IsNullOrWhiteSpace(txtLocalizacion.DevuelvoValor()))
            {
                foreach (Control c in this.Controls)
                {
                    if (c is miControl)
                    {
                        ((miControl)c).Validar();
                    }
                }

                MessageBox.Show("POR FAVOR COMPLETE LOS CAMPOS RESALTADOS");
            }
            else
            { 
                BE.Destino destino = new BE.Destino();
                destino.Nombre = txtNombre.DevuelvoValor();
                destino.Pais = txtPais.DevuelvoValor();
                destino.Localizacion = txtLocalizacion.DevuelvoValor();
                gestorDestino.Insertar(destino);
                EnlazarDestinos();
                LimpiarTextBox();
            }
        }

        private void dgDestinos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(dgDestinos.SelectedRows.Count > 0)
            {
                BE.Destino destino = new BE.Destino();
                destino.Id = int.Parse(dgDestinos.Rows[e.RowIndex].Cells[0].Value.ToString());
                destino.Nombre = dgDestinos.Rows[e.RowIndex].Cells[1].Value.ToString();
                destino.Pais = dgDestinos.Rows[e.RowIndex].Cells[2].Value.ToString();
                destino.Localizacion = dgDestinos.Rows[e.RowIndex].Cells[3].Value.ToString();

                txtNombre.LlenarCampo(destino.Nombre);
                txtPais.LlenarCampo(destino.Pais);
                txtLocalizacion.LlenarCampo(destino.Localizacion);
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            BE.Destino destino = (BE.Destino)dgDestinos.SelectedRows[0].DataBoundItem;
            destino.Nombre = txtNombre.DevuelvoValor();
            destino.Pais = txtPais.DevuelvoValor();
            destino.Localizacion = txtLocalizacion.DevuelvoValor();
            gestorDestino.Insertar(destino);
            EnlazarDestinos();
            LimpiarTextBox();
        }

        void LimpiarTextBox()
        {
            txtNombre.Limpiar();
            txtPais.Limpiar();
            txtLocalizacion.Limpiar();
            dgDestinos.ClearSelection();
            txtNombre.Focus();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            BE.Destino destino = (BE.Destino)dgDestinos.SelectedRows[0].DataBoundItem;
            gestorDestino.Eliminar(destino);
            EnlazarDestinos();
            LimpiarTextBox();
        }

        private void btnAbrirUbicacion_Click(object sender, EventArgs e)
        {
            BE.Destino d = (BE.Destino)dgDestinos.SelectedRows[0].DataBoundItem;
            Process.Start("chrome", d.Localizacion);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarTextBox();
        }
    }
}
