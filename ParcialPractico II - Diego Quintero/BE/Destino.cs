﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Destino
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string pais;

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        private string localizacion;

        public string Localizacion
        {
            get { return localizacion; }
            set { localizacion = value; }
        }


        public override string ToString()
        {
            return Nombre + ", "+ Pais;
        }
    }
}
