﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class MP_Ano
    {
        private Acceso acceso = new Acceso();

        public List<BE.Ano> Listar()
        {
            List<BE.Ano> anos = new List<BE.Ano>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_anos_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Ano a = new BE.Ano();
                a.Id = int.Parse(registro[0].ToString());
                a.Nombre = int.Parse(registro[1].ToString());
                anos.Add(a);
            }

            acceso.Cerrar();

            return anos;
        }
    }
}
