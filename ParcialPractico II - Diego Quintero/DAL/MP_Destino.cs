﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class MP_Destino
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Destino d)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@nombre", d.Nombre));
            parametros.Add(acceso.CrearParametro("@pais", d.Pais));
            parametros.Add(acceso.CrearParametro("@localizacion", d.Localizacion));
            acceso.Escribir("sp_destinos_insertar", parametros);

            acceso.Cerrar();
        }

        public void Actualizar(BE.Destino d)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", d.Id));
            parametros.Add(acceso.CrearParametro("@nombre", d.Nombre));
            parametros.Add(acceso.CrearParametro("@pais", d.Pais));
            parametros.Add(acceso.CrearParametro("@localizacion", d.Localizacion));
            acceso.Escribir("sp_destinos_actualizar", parametros);

            acceso.Cerrar();
        }

        public void Eliminar(BE.Destino d)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", d.Id));
            acceso.Escribir("sp_destinos_eliminar", parametros);

            acceso.Cerrar();
        }

        public List<BE.Destino> Listar()
        {
            List<BE.Destino> destinos = new List<BE.Destino>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_destinos_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Destino d = new BE.Destino();
                d.Id = int.Parse(registro[0].ToString());
                d.Nombre = registro[1].ToString();
                d.Pais = registro[2].ToString();
                d.Localizacion = registro[3].ToString();
                destinos.Add(d);
            }

            acceso.Cerrar();

            return destinos;
        }
    }
}
