﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmInsertarJugadores : Form
    {
        BLL.Jugador gestorJugador = new BLL.Jugador();
        BLL.Partida gestorPartida = new BLL.Partida();
        int jugadores = 0;
        int idJugador1 = 0;
        int idJugador2 = 0;
        public frmInsertarJugadores()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            BE.Jugador j = new BE.Jugador();
            j.Nombre = txtNombre.Text;
            gestorJugador.Insertar(j);
            jugadores++;
            txtNombre.Clear();
            txtNombre.Focus();

            if(jugadores == 1)
            {
                idJugador1 = gestorJugador.ObtenerIdJugador();
                lblLeyenda.Text = "INGRESE JUGADOR 2";
            }

            if(jugadores == 2)
            {
                idJugador2 = gestorJugador.ObtenerIdJugador();
                gestorPartida.InsertarPartida(idJugador1, idJugador2);
                frmJuego frmJuego = new frmJuego();
                this.Hide();
                frmJuego.Show();
            }
        }

        private void frmInsertarJugadores_Load(object sender, EventArgs e)
        {

        }
    }
}
