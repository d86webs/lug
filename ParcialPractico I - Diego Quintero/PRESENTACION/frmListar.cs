﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmListar : Form
    {
        BLL.Recibo gestorRecibo = new BLL.Recibo();
        BLL.Empleado gestorEmpleado = new BLL.Empleado();
        BLL.Mes gestorMeses = new BLL.Mes();
        BLL.Ano gestorAnos = new BLL.Ano();
        BLL.Concepto gestorConceptos = new BLL.Concepto();
        public frmListar()
        {
            InitializeComponent();
        }

        private void frmListar_Load(object sender, EventArgs e)
        {
            EnlazarRecibos();
            EnlazarCombos();
            txtSueldoBruto.Enabled = false;
            txtDescuentos.Enabled = false;
            txtSueldoNeto.Enabled = false;
        }

        void EnlazarRecibos()
        {
            dgListar.DataSource = null;
            dgListar.DataSource = gestorRecibo.ListarRecibos();
        }

        void EnlazarCombos()
        {
            cboEmpleados.DataSource = null;
            cboMeses.DataSource = null;
            cboAno.DataSource = null;
            cboEmpleados.Items.AddRange(gestorEmpleado.ListarEmpleados().ToArray());
            cboAno.Items.AddRange(gestorAnos.ListarAnos().ToArray());
            cboMeses.Items.AddRange(gestorMeses.ListarMeses().ToArray());
        }

        private void cboEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            BE.Empleado empleado = (BE.Empleado)cboEmpleados.SelectedItem;
            dgListar.DataSource = null;
            dgListar.DataSource = gestorEmpleado.ListarRecibosPorEmpleado(empleado);
            txtSueldoBruto.Enabled = false;
            txtDescuentos.Enabled = false;
            txtSueldoNeto.Enabled = false;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            txtSueldoBruto.Enabled = true;
            txtDescuentos.Enabled = true;
            txtSueldoNeto.Enabled = true;
            BE.Empleado empleado = (BE.Empleado)cboEmpleados.SelectedItem;
            BE.Ano ano = (BE.Ano)cboAno.SelectedItem;
            BE.Mes mes = (BE.Mes)cboMeses.SelectedItem;
            dgListar.DataSource = null;
            EnlazarConceptos();
            BE.Recibo recibo = gestorEmpleado.ListarReciboPorFecha(empleado, ano, mes);
            txtSueldoBruto.Text = recibo.Bruto.ToString();
            txtDescuentos.Text = recibo.Descuento.ToString();
            txtSueldoNeto.Text = recibo.Neto.ToString();
        }

        void EnlazarConceptos()
        {
            dgListar.DataSource = null;
            dgListar.DataSource = gestorConceptos.ListarConceptos();
        }

        private void frmListar_Activated(object sender, EventArgs e)
        {
            EnlazarCombos();
            EnlazarRecibos();
        }
    }
}
