﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Partida
    {
        private int id_partida;

        public int Id_partida
        {
            get { return id_partida; }
            set { id_partida = value; }
        }

        private Jugador id_jugador;

        public Jugador Id_jugador
        {
            get { return id_jugador; }
            set { id_jugador = value; }
        }


        private int uno;

        public int Uno
        {
            get { return uno; }
            set { uno = value; }
        }

        private int dos;

        public int Dos
        {
            get { return dos; }
            set { dos = value; }
        }

        private int tres;

        public int Tres
        {
            get { return tres; }
            set { tres = value; }
        }

        private int cuatro;

        public int Cuatro
        {
            get { return cuatro; }
            set { cuatro = value; }
        }


        private int cinco;

        public int Cinco
        {
            get { return cinco; }
            set { cinco = value; }
        }

        private int seis;

        public int Seis
        {
            get { return seis; }
            set { seis = value; }
        }

        private int escalera;

        public int Escalera
        {
            get { return escalera; }
            set { escalera = value; }
        }

        private int full;

        public int Full
        {
            get { return full; }
            set { full = value; }
        }

        private int poker;

        public int Poker
        {
            get { return poker; }
            set { poker = value; }
        }

        private int generala;

        public int Generala
        {
            get { return generala; }
            set { generala = value; }
        }

        private int generalaDoble;

        public int GeneralaDoble
        {
            get { return generalaDoble; }
            set { generalaDoble = value; }
        }

        private int total;

        public int Total
        {
            get { return total; }
            set { total = value; }
        }

    }
}
