USE [GENERALA]
GO
/****** Object:  Table [dbo].[tb_jugadores]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_jugadores](
	[id_jugador] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_partidas]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_partidas](
	[id_partida] [int] NOT NULL,
	[id_jugador] [int] NULL,
	[UNO] [int] NULL,
	[DOS] [int] NULL,
	[TRES] [int] NULL,
	[CUATRO] [int] NULL,
	[CINCO] [int] NULL,
	[SEIS] [int] NULL,
	[ESCALERA] [int] NULL,
	[FULL] [int] NULL,
	[POKER] [int] NULL,
	[GENERALA] [int] NULL,
	[GENERALA DOBLE] [int] NULL,
	[TOTAL] [int] NULL,
 CONSTRAINT [PK_tb_partidas] PRIMARY KEY CLUSTERED 
(
	[id_partida] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_puntajes]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_puntajes](
	[id_puntaje] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[valor] [int] NOT NULL,
	[servido] [int] NULL,
 CONSTRAINT [PK_tb_puntajes] PRIMARY KEY CLUSTERED 
(
	[id_puntaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_jugador_borrar]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_jugador_borrar]
	@id int
as
begin
	delete from tb_jugadores where id_jugador = @id
end
GO
/****** Object:  StoredProcedure [dbo].[sp_jugador_insertar]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_jugador_insertar]
	@nombre varchar(50)
as
begin
	declare @id int
	set @id = isnull((select max(id_jugador) from tb_jugadores), 0) + 1

	insert into tb_jugadores values (@id, @nombre)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_jugador_listar]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_jugador_listar]
	@id int
as
begin
	select * from tb_jugadores order by id_jugador
end
GO
/****** Object:  StoredProcedure [dbo].[sp_jugador_modificar]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_jugador_modificar]
	@id int,
	@nombre varchar(50)
as
begin
	update tb_jugadores set nombre = @nombre where id_jugador = @id 
end
GO
/****** Object:  StoredProcedure [dbo].[sp_jugador_obtener_id]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_jugador_obtener_id]
as
begin
	select max(id_jugador) from tb_jugadores
end
GO
/****** Object:  StoredProcedure [dbo].[sp_listar_puntajes]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_listar_puntajes]
	@tipo int
as
if @tipo = 1 begin
	select id_puntaje, nombre, valor, servido from tb_puntajes order by id_puntaje
end
GO
/****** Object:  StoredProcedure [dbo].[sp_partidas_insertar]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec sp_partidas_insertar 25, 26
CREATE proc [dbo].[sp_partidas_insertar]
 @id_jugador_1 int,
 @id_jugador_2 int
as
begin
	declare @id int
	set @id = (select isnull(max(id_partida), 0) + 1 from tb_partidas)

	insert into tb_partidas values (@id, @id_jugador_1, null, null, null, null, null, null, null, null, null, null, null, null)

	set @id = (select isnull(max(id_partida), 0) + 1 from tb_partidas)

	insert into tb_partidas values (@id, @id_jugador_2, null, null, null, null, null, null, null, null, null, null, null, null)
end
GO
/****** Object:  StoredProcedure [dbo].[sp_partidas_listar]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_partidas_listar]
as
begin

	select top 2 a.id_partida, a.id_jugador, b.nombre JUGADOR, a.UNO, a.DOS, a.TRES, a.CUATRO, a.CINCO, a.SEIS, a.ESCALERA, a.[FULL], a.POKER, a.GENERALA, a.[GENERALA DOBLE]
	from tb_partidas a
	left join tb_jugadores b on b.id_jugador = a.id_jugador
	order by a.id_partida desc

end
GO
/****** Object:  StoredProcedure [dbo].[sp_partidas_obtener]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_partidas_obtener]
as
begin
	select isnull(max(id_partida), 0) + 1 from tb_partidas
end
GO
/****** Object:  StoredProcedure [dbo].[sp_puntajes_obtener]    Script Date: 17/11/2020 19:42:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_puntajes_obtener]
	@id_puntaje int,
	@id_tipo int
as
begin
	if @id_tipo = 1 begin
		select valor from tb_puntajes where id_puntaje = @id_puntaje
	end

	if @id_tipo = 2 begin
		select servido from tb_puntajes where id_puntaje = @id_puntaje
	end
end
GO
