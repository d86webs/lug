﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Vuelo
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Vuelo v)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id_destino", v.Destino.Id));
            parametros.Add(acceso.CrearParametro("@partida", v.Partida));
            parametros.Add(acceso.CrearParametro("@llegada", v.Llegada));
            parametros.Add(acceso.CrearParametro("@capacidad", v.Capacidad));
            parametros.Add(acceso.CrearParametro("@ocupados", v.Ocupados));
            parametros.Add(acceso.CrearParametro("@tipo", v.Tipo));
            acceso.Escribir("sp_vuelos_insertar", parametros);

            acceso.Cerrar();
        }

        public void Actualizar(BE.Vuelo v)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", v.Id));
            parametros.Add(acceso.CrearParametro("@id_destino", v.Destino.Id));
            parametros.Add(acceso.CrearParametro("@partida", v.Partida));
            parametros.Add(acceso.CrearParametro("@llegada", v.Llegada));
            parametros.Add(acceso.CrearParametro("@capacidad", v.Capacidad));
            parametros.Add(acceso.CrearParametro("@ocupados", v.Ocupados));
            parametros.Add(acceso.CrearParametro("@tipo", v.Tipo));
            acceso.Escribir("sp_vuelos_actualizar", parametros);

            acceso.Cerrar();
        }

        public void Eliminar(BE.Vuelo v)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", v.Id));
            acceso.Escribir("sp_vuelos_eliminar", parametros);

            acceso.Cerrar();
        }

        public List<BE.Vuelo> Listar()
        {
            List<BE.Vuelo> vuelos = new List<BE.Vuelo>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_vuelos_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Vuelo v = new BE.Vuelo();
                v.Id = int.Parse(registro[0].ToString());
                v.Destino.Nombre = registro[1].ToString();
                v.Destino.Pais = registro[2].ToString();
                v.Partida = DateTime.Parse(registro[3].ToString());
                v.Llegada = DateTime.Parse(registro[4].ToString());
                v.Capacidad = int.Parse(registro[5].ToString());
                v.Ocupados = int.Parse(registro[6].ToString());
                v.Tipo = registro[7].ToString();
                vuelos.Add(v);
            }

            acceso.Cerrar();

            return vuelos;
        }

        public DataTable ListarVuelosPorDestinos(BE.Destino d)
        {
            DataTable dtVuelosPorDestinos = new DataTable();

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@tipo", 2));
            parametros.Add(acceso.CrearParametro("@id_destino", d.Id));

            string SQL = string.Format("sp_vuelos_listar");

            dtVuelosPorDestinos = acceso.Leer(SQL, parametros); 

            acceso.Cerrar();

            return dtVuelosPorDestinos;
        }

        public DataTable VuelosPorDestinos(int id)
        {
            DataTable vuelos = new DataTable();

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_destino", id));

            string SQL = string.Format("sp_vuelos_listarPorDestino");

            vuelos = acceso.Leer(SQL, parametros);

            acceso.Cerrar();

            return vuelos;
        }

        public DataTable VuelosPorFechaPartida(DateTime fecha)
        {
            DataTable vuelos = new DataTable();

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@fecha", fecha));

            string SQL = string.Format("sp_vuelos_listarPorFechaPartida");

            vuelos = acceso.Leer(SQL, parametros);

            acceso.Cerrar();

            return vuelos;
        }

        public DataTable VuelosPorFechaLlegada(DateTime fecha)
        {
            DataTable vuelos = new DataTable();

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@fecha", fecha));

            string SQL = string.Format("sp_vuelos_listarPorFechaLlegada");

            vuelos = acceso.Leer(SQL, parametros);

            acceso.Cerrar();

            return vuelos;
        }

        public DataTable VuelosNacionales()
        {
            DataTable vuelos = new DataTable();

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            string SQL = string.Format("sp_vuelos_listarPorTipoNacional");

            vuelos = acceso.Leer(SQL);

            acceso.Cerrar();

            return vuelos;
        }

        public DataTable VuelosInternacionales()
        {
            DataTable vuelos = new DataTable();

            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            string SQL = string.Format("sp_vuelos_listarPorTipoInternacional");

            vuelos = acceso.Leer(SQL);

            acceso.Cerrar();

            return vuelos;
        }

        public DataTable VuelosFinalizadosOcupadocionMenorA40()
        {
            DataTable vuelos = new DataTable();

            acceso.Abrir();

            string SQL = string.Format("sp_vuelos_listar_ocupacion_menor_a_40");

            vuelos = acceso.Leer(SQL);

            acceso.Cerrar();

            return vuelos;
        }
    }
}
