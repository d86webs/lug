﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class MP_Mes
    {
        private Acceso acceso = new Acceso();

        public List<BE.Mes> Listar()
        {
            List<BE.Mes> meses = new List<BE.Mes>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_meses_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Mes m = new BE.Mes();
                m.Id = int.Parse(registro[0].ToString());
                m.Nombre = registro[1].ToString();
                meses.Add(m);
            }

            acceso.Cerrar();

            return meses;
        }
    }
}
