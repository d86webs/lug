﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class MP_Pasajero
    {
        private Acceso acceso = new Acceso();

        public void Insertar(BE.Pasajero p)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@nombre", p.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", p.Apellido));
            parametros.Add(acceso.CrearParametro("@dni", p.Dni));
            acceso.Escribir("sp_pasajeros_insertar", parametros);

            acceso.Cerrar();
        }

        public void Actualizar(BE.Pasajero p)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", p.Id));
            parametros.Add(acceso.CrearParametro("@nombre", p.Nombre));
            parametros.Add(acceso.CrearParametro("@apellido", p.Apellido));
            parametros.Add(acceso.CrearParametro("@dni", p.Dni));
            acceso.Escribir("sp_pasajeros_actualizar", parametros);

            acceso.Cerrar();
        }

        public void Eliminar(BE.Pasajero p)
        {
            acceso.Abrir();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", p.Id));
            acceso.Escribir("sp_pasajeros_eliminar", parametros);

            acceso.Cerrar();
        }

        public List<BE.Pasajero> Listar()
        {
            List<BE.Pasajero> pasajeros = new List<BE.Pasajero>();

            acceso.Abrir();

            DataTable tabla = acceso.Leer("sp_pasajeros_listar");

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Pasajero p = new BE.Pasajero();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                p.Apellido = registro[2].ToString();
                p.Dni = int.Parse(registro[3].ToString());
                pasajeros.Add(p);
            }

            acceso.Cerrar();

            return pasajeros;
        }
    }
}
