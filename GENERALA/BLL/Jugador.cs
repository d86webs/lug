﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugador
    {
        DAL.MP_Jugador mp = new DAL.MP_Jugador();

        public void Insertar(BE.Jugador j)
        {
            if(j.Id == 0)
            {
                mp.Insertar(j);
            }
            else
            {
                mp.Modificar(j);
            }
        }

        public void Borrar(BE.Jugador j)
        {
            mp.Borrar(j);
        }

        public List<BE.Jugador> ListarJugadores()
        {
            return mp.Listar();
        }

        public int ObtenerIdJugador()
        {
            return mp.ObtenerIdJugador();
        }
    }
}
