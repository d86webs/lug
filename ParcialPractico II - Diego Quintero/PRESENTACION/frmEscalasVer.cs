﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmEscalasVer : Form
    {
        BLL.Escala gestorEscala = new BLL.Escala();
        int id;
        public frmEscalasVer()
        {
            InitializeComponent();
        }

        private void btnCerar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEscalasVer_Load(object sender, EventArgs e)
        {
            dgEscalas.DataSource = null;
            dgEscalas.DataSource = gestorEscala.ListarEscalas(id);
            dgEscalas.Columns[0].Visible = false;
            dgEscalas.ClearSelection();
        }

        public void EnviarVuelo(int id_vuelo)
        {
            id = id_vuelo;
        }
    }
}
