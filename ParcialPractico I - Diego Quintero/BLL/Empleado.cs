﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Empleado
    {
        DAL.MP_Empleado mp = new DAL.MP_Empleado();

        public void Insertar(BE.Empleado e)
        {
            if(e.Legajo == 0)
            {
                mp.Insertar(e);
            }
            else
            {
                mp.Modificar(e);
            }
        }

        public void Eliminar(BE.Empleado e)
        {
            mp.Eliminar(e);
        }

        public List<BE.Empleado> ListarEmpleados()
        {
            return mp.Listar();
        }

        public List<BE.Recibo> ListarRecibosPorEmpleado(BE.Empleado empleado)
        {
            return mp.ListarRecibosPorEmpleado(empleado);
        }

        public BE.Recibo ListarReciboPorFecha(BE.Empleado e, BE.Ano a, BE.Mes m)
        {
            return mp.ListarRecibosPorFecha(e, a, m);
        }
    }
}
