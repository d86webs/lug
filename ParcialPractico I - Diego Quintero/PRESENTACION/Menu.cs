﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void eMPLEADOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEmpleados frmEmpleados = new frmEmpleados();
            frmEmpleados.MdiParent = this;
            frmEmpleados.Show();
        }

        private void sALIRToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cONCEPTOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConceptos frmConceptos = new frmConceptos();
            frmConceptos.MdiParent = this;
            frmConceptos.Show();
        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void lIQUIDARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLiquidar frmLiquidar = new frmLiquidar();
            frmLiquidar.MdiParent = this;
            frmLiquidar.Show();
        }

        private void lISTARToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmListar frmListar = new frmListar();
            frmListar.MdiParent = this;
            frmListar.Show();
        }
    }
}
