﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BLL
{
    public class Partida
    {
        DAL.MP_Partida mp = new DAL.MP_Partida();

        public DataTable ListarPartida()
        {
            return mp.ListarPartida();
        }

        public int ObtenerIdPartida()
        {
            return mp.ObtenerIdPartida();
        }

        public void InsertarPartida(int id_jugador1, int id_jugador2)
        {
            mp.InsertarPartida(id_jugador1, id_jugador2);
        }
    }
}
