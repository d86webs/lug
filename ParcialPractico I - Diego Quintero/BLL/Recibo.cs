﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Recibo
    {
        DAL.MP_Recibo mp = new DAL.MP_Recibo();

        public void Insertar(BE.Recibo r)
        {
            mp.Insertar(r);
        }

        public List<BE.Recibo> ListarRecibos()
        {
            return mp.Listar();
        }
    }
}
