﻿namespace PRESENTACION
{
    partial class frmJuego
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnLanzar = new System.Windows.Forms.Button();
            this.btnDado2 = new System.Windows.Forms.Button();
            this.btnDado3 = new System.Windows.Forms.Button();
            this.btnDado4 = new System.Windows.Forms.Button();
            this.btnDado5 = new System.Windows.Forms.Button();
            this.grbMesa = new System.Windows.Forms.GroupBox();
            this.btnDado1 = new System.Windows.Forms.Button();
            this.gbSeleccion = new System.Windows.Forms.GroupBox();
            this.btnDadoSel4 = new System.Windows.Forms.Button();
            this.btnDadoSel5 = new System.Windows.Forms.Button();
            this.btnDadoSel3 = new System.Windows.Forms.Button();
            this.btnDadoSel1 = new System.Windows.Forms.Button();
            this.btnDadoSel2 = new System.Windows.Forms.Button();
            this.lblDadosSeleccionados = new System.Windows.Forms.Label();
            this.lblTirosDisponibles = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAnotar = new System.Windows.Forms.Button();
            this.txtGeneralaDoble = new System.Windows.Forms.TextBox();
            this.rdbGeneralaDoble = new System.Windows.Forms.RadioButton();
            this.txtGenerala = new System.Windows.Forms.TextBox();
            this.txtPoker = new System.Windows.Forms.TextBox();
            this.txtFull = new System.Windows.Forms.TextBox();
            this.txtEscalera = new System.Windows.Forms.TextBox();
            this.txtSeis = new System.Windows.Forms.TextBox();
            this.txtCinco = new System.Windows.Forms.TextBox();
            this.txtCuatro = new System.Windows.Forms.TextBox();
            this.txtTres = new System.Windows.Forms.TextBox();
            this.txtDos = new System.Windows.Forms.TextBox();
            this.txtUno = new System.Windows.Forms.TextBox();
            this.rdbGenerala = new System.Windows.Forms.RadioButton();
            this.rdbPoker = new System.Windows.Forms.RadioButton();
            this.rdbFull = new System.Windows.Forms.RadioButton();
            this.rdbEscalera = new System.Windows.Forms.RadioButton();
            this.rdbSeis = new System.Windows.Forms.RadioButton();
            this.rdbCinco = new System.Windows.Forms.RadioButton();
            this.rdbCuatro = new System.Windows.Forms.RadioButton();
            this.rdbTres = new System.Windows.Forms.RadioButton();
            this.rdbDos = new System.Windows.Forms.RadioButton();
            this.rdbUno = new System.Windows.Forms.RadioButton();
            this.grbMesa.SuspendLayout();
            this.gbSeleccion.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnLanzar
            // 
            this.btnLanzar.BackColor = System.Drawing.Color.SaddleBrown;
            this.btnLanzar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLanzar.ForeColor = System.Drawing.SystemColors.Window;
            this.btnLanzar.Location = new System.Drawing.Point(352, 125);
            this.btnLanzar.Name = "btnLanzar";
            this.btnLanzar.Size = new System.Drawing.Size(111, 72);
            this.btnLanzar.TabIndex = 10;
            this.btnLanzar.Text = "LANZAR!!!";
            this.btnLanzar.UseVisualStyleBackColor = false;
            this.btnLanzar.Click += new System.EventHandler(this.btnLanzar_Click);
            // 
            // btnDado2
            // 
            this.btnDado2.BackColor = System.Drawing.SystemColors.Window;
            this.btnDado2.Location = new System.Drawing.Point(64, 173);
            this.btnDado2.Name = "btnDado2";
            this.btnDado2.Size = new System.Drawing.Size(59, 53);
            this.btnDado2.TabIndex = 12;
            this.btnDado2.UseVisualStyleBackColor = false;
            this.btnDado2.Click += new System.EventHandler(this.btnDado2_Click);
            // 
            // btnDado3
            // 
            this.btnDado3.BackColor = System.Drawing.SystemColors.Window;
            this.btnDado3.Location = new System.Drawing.Point(108, 101);
            this.btnDado3.Name = "btnDado3";
            this.btnDado3.Size = new System.Drawing.Size(59, 53);
            this.btnDado3.TabIndex = 13;
            this.btnDado3.UseVisualStyleBackColor = false;
            this.btnDado3.Click += new System.EventHandler(this.btnDado3_Click);
            // 
            // btnDado4
            // 
            this.btnDado4.BackColor = System.Drawing.SystemColors.Window;
            this.btnDado4.Location = new System.Drawing.Point(194, 101);
            this.btnDado4.Name = "btnDado4";
            this.btnDado4.Size = new System.Drawing.Size(59, 53);
            this.btnDado4.TabIndex = 14;
            this.btnDado4.UseVisualStyleBackColor = false;
            this.btnDado4.Click += new System.EventHandler(this.btnDado4_Click);
            // 
            // btnDado5
            // 
            this.btnDado5.BackColor = System.Drawing.SystemColors.Window;
            this.btnDado5.Location = new System.Drawing.Point(236, 173);
            this.btnDado5.Name = "btnDado5";
            this.btnDado5.Size = new System.Drawing.Size(59, 53);
            this.btnDado5.TabIndex = 15;
            this.btnDado5.UseVisualStyleBackColor = false;
            this.btnDado5.Click += new System.EventHandler(this.btnDado5_Click);
            // 
            // grbMesa
            // 
            this.grbMesa.BackColor = System.Drawing.Color.Transparent;
            this.grbMesa.Controls.Add(this.btnDado1);
            this.grbMesa.Controls.Add(this.gbSeleccion);
            this.grbMesa.Controls.Add(this.lblDadosSeleccionados);
            this.grbMesa.Controls.Add(this.lblTirosDisponibles);
            this.grbMesa.Controls.Add(this.btnDado5);
            this.grbMesa.Controls.Add(this.btnLanzar);
            this.grbMesa.Controls.Add(this.btnDado2);
            this.grbMesa.Controls.Add(this.btnDado3);
            this.grbMesa.Controls.Add(this.btnDado4);
            this.grbMesa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbMesa.Location = new System.Drawing.Point(12, 12);
            this.grbMesa.Name = "grbMesa";
            this.grbMesa.Size = new System.Drawing.Size(516, 371);
            this.grbMesa.TabIndex = 21;
            this.grbMesa.TabStop = false;
            this.grbMesa.Text = "MESA";
            // 
            // btnDado1
            // 
            this.btnDado1.BackColor = System.Drawing.SystemColors.Window;
            this.btnDado1.Location = new System.Drawing.Point(150, 173);
            this.btnDado1.Name = "btnDado1";
            this.btnDado1.Size = new System.Drawing.Size(59, 53);
            this.btnDado1.TabIndex = 24;
            this.btnDado1.UseVisualStyleBackColor = false;
            this.btnDado1.Click += new System.EventHandler(this.btnDado1_Click_1);
            // 
            // gbSeleccion
            // 
            this.gbSeleccion.Controls.Add(this.btnDadoSel4);
            this.gbSeleccion.Controls.Add(this.btnDadoSel5);
            this.gbSeleccion.Controls.Add(this.btnDadoSel3);
            this.gbSeleccion.Controls.Add(this.btnDadoSel1);
            this.gbSeleccion.Controls.Add(this.btnDadoSel2);
            this.gbSeleccion.Location = new System.Drawing.Point(22, 246);
            this.gbSeleccion.Name = "gbSeleccion";
            this.gbSeleccion.Size = new System.Drawing.Size(469, 117);
            this.gbSeleccion.TabIndex = 23;
            this.gbSeleccion.TabStop = false;
            this.gbSeleccion.Text = "DADOS SELECCIONADOS";
            // 
            // btnDadoSel4
            // 
            this.btnDadoSel4.BackColor = System.Drawing.SystemColors.Window;
            this.btnDadoSel4.Enabled = false;
            this.btnDadoSel4.Location = new System.Drawing.Point(282, 35);
            this.btnDadoSel4.Name = "btnDadoSel4";
            this.btnDadoSel4.Size = new System.Drawing.Size(59, 53);
            this.btnDadoSel4.TabIndex = 27;
            this.btnDadoSel4.UseVisualStyleBackColor = false;
            // 
            // btnDadoSel5
            // 
            this.btnDadoSel5.BackColor = System.Drawing.SystemColors.Window;
            this.btnDadoSel5.Enabled = false;
            this.btnDadoSel5.Location = new System.Drawing.Point(359, 35);
            this.btnDadoSel5.Name = "btnDadoSel5";
            this.btnDadoSel5.Size = new System.Drawing.Size(59, 53);
            this.btnDadoSel5.TabIndex = 26;
            this.btnDadoSel5.UseVisualStyleBackColor = false;
            // 
            // btnDadoSel3
            // 
            this.btnDadoSel3.BackColor = System.Drawing.SystemColors.Window;
            this.btnDadoSel3.Enabled = false;
            this.btnDadoSel3.Location = new System.Drawing.Point(204, 35);
            this.btnDadoSel3.Name = "btnDadoSel3";
            this.btnDadoSel3.Size = new System.Drawing.Size(59, 53);
            this.btnDadoSel3.TabIndex = 23;
            this.btnDadoSel3.UseVisualStyleBackColor = false;
            // 
            // btnDadoSel1
            // 
            this.btnDadoSel1.BackColor = System.Drawing.SystemColors.Window;
            this.btnDadoSel1.Enabled = false;
            this.btnDadoSel1.Location = new System.Drawing.Point(50, 34);
            this.btnDadoSel1.Name = "btnDadoSel1";
            this.btnDadoSel1.Size = new System.Drawing.Size(59, 53);
            this.btnDadoSel1.TabIndex = 25;
            this.btnDadoSel1.UseVisualStyleBackColor = false;
            // 
            // btnDadoSel2
            // 
            this.btnDadoSel2.BackColor = System.Drawing.SystemColors.Window;
            this.btnDadoSel2.Enabled = false;
            this.btnDadoSel2.Location = new System.Drawing.Point(128, 35);
            this.btnDadoSel2.Name = "btnDadoSel2";
            this.btnDadoSel2.Size = new System.Drawing.Size(59, 53);
            this.btnDadoSel2.TabIndex = 24;
            this.btnDadoSel2.UseVisualStyleBackColor = false;
            // 
            // lblDadosSeleccionados
            // 
            this.lblDadosSeleccionados.AutoSize = true;
            this.lblDadosSeleccionados.Location = new System.Drawing.Point(19, 52);
            this.lblDadosSeleccionados.Name = "lblDadosSeleccionados";
            this.lblDadosSeleccionados.Size = new System.Drawing.Size(165, 13);
            this.lblDadosSeleccionados.TabIndex = 22;
            this.lblDadosSeleccionados.Text = "DADOS SELECCIONADOS: ";
            // 
            // lblTirosDisponibles
            // 
            this.lblTirosDisponibles.AutoSize = true;
            this.lblTirosDisponibles.Location = new System.Drawing.Point(19, 29);
            this.lblTirosDisponibles.Name = "lblTirosDisponibles";
            this.lblTirosDisponibles.Size = new System.Drawing.Size(139, 13);
            this.lblTirosDisponibles.TabIndex = 22;
            this.lblTirosDisponibles.Text = "TIROS DISPONIBLES: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(12, 389);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(984, 151);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "PUNTAJES";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(6, 31);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(972, 114);
            this.dataGridView1.TabIndex = 27;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnAnotar);
            this.groupBox3.Controls.Add(this.txtGeneralaDoble);
            this.groupBox3.Controls.Add(this.rdbGeneralaDoble);
            this.groupBox3.Controls.Add(this.txtGenerala);
            this.groupBox3.Controls.Add(this.txtPoker);
            this.groupBox3.Controls.Add(this.txtFull);
            this.groupBox3.Controls.Add(this.txtEscalera);
            this.groupBox3.Controls.Add(this.txtSeis);
            this.groupBox3.Controls.Add(this.txtCinco);
            this.groupBox3.Controls.Add(this.txtCuatro);
            this.groupBox3.Controls.Add(this.txtTres);
            this.groupBox3.Controls.Add(this.txtDos);
            this.groupBox3.Controls.Add(this.txtUno);
            this.groupBox3.Controls.Add(this.rdbGenerala);
            this.groupBox3.Controls.Add(this.rdbPoker);
            this.groupBox3.Controls.Add(this.rdbFull);
            this.groupBox3.Controls.Add(this.rdbEscalera);
            this.groupBox3.Controls.Add(this.rdbSeis);
            this.groupBox3.Controls.Add(this.rdbCinco);
            this.groupBox3.Controls.Add(this.rdbCuatro);
            this.groupBox3.Controls.Add(this.rdbTres);
            this.groupBox3.Controls.Add(this.rdbDos);
            this.groupBox3.Controls.Add(this.rdbUno);
            this.groupBox3.Location = new System.Drawing.Point(534, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(462, 371);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "PUNTAJES POSIBLES";
            this.groupBox3.Enter += new System.EventHandler(this.groupBox3_Enter);
            // 
            // btnAnotar
            // 
            this.btnAnotar.Location = new System.Drawing.Point(277, 153);
            this.btnAnotar.Name = "btnAnotar";
            this.btnAnotar.Size = new System.Drawing.Size(103, 50);
            this.btnAnotar.TabIndex = 46;
            this.btnAnotar.Text = "ANOTAR";
            this.btnAnotar.UseVisualStyleBackColor = true;
            // 
            // txtGeneralaDoble
            // 
            this.txtGeneralaDoble.Location = new System.Drawing.Point(208, 309);
            this.txtGeneralaDoble.Name = "txtGeneralaDoble";
            this.txtGeneralaDoble.ReadOnly = true;
            this.txtGeneralaDoble.Size = new System.Drawing.Size(41, 20);
            this.txtGeneralaDoble.TabIndex = 45;
            // 
            // rdbGeneralaDoble
            // 
            this.rdbGeneralaDoble.AutoSize = true;
            this.rdbGeneralaDoble.Location = new System.Drawing.Point(83, 310);
            this.rdbGeneralaDoble.Name = "rdbGeneralaDoble";
            this.rdbGeneralaDoble.Size = new System.Drawing.Size(122, 17);
            this.rdbGeneralaDoble.TabIndex = 44;
            this.rdbGeneralaDoble.TabStop = true;
            this.rdbGeneralaDoble.Text = "GENERALA DOBLE";
            this.rdbGeneralaDoble.UseVisualStyleBackColor = true;
            // 
            // txtGenerala
            // 
            this.txtGenerala.Location = new System.Drawing.Point(207, 284);
            this.txtGenerala.Name = "txtGenerala";
            this.txtGenerala.ReadOnly = true;
            this.txtGenerala.Size = new System.Drawing.Size(41, 20);
            this.txtGenerala.TabIndex = 43;
            // 
            // txtPoker
            // 
            this.txtPoker.Location = new System.Drawing.Point(207, 258);
            this.txtPoker.Name = "txtPoker";
            this.txtPoker.ReadOnly = true;
            this.txtPoker.Size = new System.Drawing.Size(41, 20);
            this.txtPoker.TabIndex = 42;
            // 
            // txtFull
            // 
            this.txtFull.Location = new System.Drawing.Point(207, 231);
            this.txtFull.Name = "txtFull";
            this.txtFull.ReadOnly = true;
            this.txtFull.Size = new System.Drawing.Size(41, 20);
            this.txtFull.TabIndex = 41;
            // 
            // txtEscalera
            // 
            this.txtEscalera.Location = new System.Drawing.Point(207, 205);
            this.txtEscalera.Name = "txtEscalera";
            this.txtEscalera.ReadOnly = true;
            this.txtEscalera.Size = new System.Drawing.Size(41, 20);
            this.txtEscalera.TabIndex = 40;
            // 
            // txtSeis
            // 
            this.txtSeis.Location = new System.Drawing.Point(207, 179);
            this.txtSeis.Name = "txtSeis";
            this.txtSeis.ReadOnly = true;
            this.txtSeis.Size = new System.Drawing.Size(41, 20);
            this.txtSeis.TabIndex = 39;
            // 
            // txtCinco
            // 
            this.txtCinco.Location = new System.Drawing.Point(207, 153);
            this.txtCinco.Name = "txtCinco";
            this.txtCinco.ReadOnly = true;
            this.txtCinco.Size = new System.Drawing.Size(41, 20);
            this.txtCinco.TabIndex = 38;
            // 
            // txtCuatro
            // 
            this.txtCuatro.Location = new System.Drawing.Point(207, 125);
            this.txtCuatro.Name = "txtCuatro";
            this.txtCuatro.ReadOnly = true;
            this.txtCuatro.Size = new System.Drawing.Size(41, 20);
            this.txtCuatro.TabIndex = 37;
            // 
            // txtTres
            // 
            this.txtTres.Location = new System.Drawing.Point(207, 99);
            this.txtTres.Name = "txtTres";
            this.txtTres.ReadOnly = true;
            this.txtTres.Size = new System.Drawing.Size(41, 20);
            this.txtTres.TabIndex = 36;
            // 
            // txtDos
            // 
            this.txtDos.Location = new System.Drawing.Point(207, 73);
            this.txtDos.Name = "txtDos";
            this.txtDos.ReadOnly = true;
            this.txtDos.Size = new System.Drawing.Size(41, 20);
            this.txtDos.TabIndex = 35;
            // 
            // txtUno
            // 
            this.txtUno.Location = new System.Drawing.Point(207, 47);
            this.txtUno.Name = "txtUno";
            this.txtUno.ReadOnly = true;
            this.txtUno.Size = new System.Drawing.Size(41, 20);
            this.txtUno.TabIndex = 25;
            // 
            // rdbGenerala
            // 
            this.rdbGenerala.AutoSize = true;
            this.rdbGenerala.Location = new System.Drawing.Point(82, 285);
            this.rdbGenerala.Name = "rdbGenerala";
            this.rdbGenerala.Size = new System.Drawing.Size(83, 17);
            this.rdbGenerala.TabIndex = 34;
            this.rdbGenerala.TabStop = true;
            this.rdbGenerala.Text = "GENERALA";
            this.rdbGenerala.UseVisualStyleBackColor = true;
            // 
            // rdbPoker
            // 
            this.rdbPoker.AutoSize = true;
            this.rdbPoker.Location = new System.Drawing.Point(82, 259);
            this.rdbPoker.Name = "rdbPoker";
            this.rdbPoker.Size = new System.Drawing.Size(62, 17);
            this.rdbPoker.TabIndex = 33;
            this.rdbPoker.TabStop = true;
            this.rdbPoker.Text = "POKER";
            this.rdbPoker.UseVisualStyleBackColor = true;
            // 
            // rdbFull
            // 
            this.rdbFull.AutoSize = true;
            this.rdbFull.Location = new System.Drawing.Point(82, 232);
            this.rdbFull.Name = "rdbFull";
            this.rdbFull.Size = new System.Drawing.Size(51, 17);
            this.rdbFull.TabIndex = 32;
            this.rdbFull.TabStop = true;
            this.rdbFull.Text = "FULL";
            this.rdbFull.UseVisualStyleBackColor = true;
            // 
            // rdbEscalera
            // 
            this.rdbEscalera.AutoSize = true;
            this.rdbEscalera.Location = new System.Drawing.Point(82, 206);
            this.rdbEscalera.Name = "rdbEscalera";
            this.rdbEscalera.Size = new System.Drawing.Size(81, 17);
            this.rdbEscalera.TabIndex = 31;
            this.rdbEscalera.TabStop = true;
            this.rdbEscalera.Text = "ESCALERA";
            this.rdbEscalera.UseVisualStyleBackColor = true;
            // 
            // rdbSeis
            // 
            this.rdbSeis.AutoSize = true;
            this.rdbSeis.Location = new System.Drawing.Point(82, 180);
            this.rdbSeis.Name = "rdbSeis";
            this.rdbSeis.Size = new System.Drawing.Size(49, 17);
            this.rdbSeis.TabIndex = 30;
            this.rdbSeis.TabStop = true;
            this.rdbSeis.Text = "SEIS";
            this.rdbSeis.UseVisualStyleBackColor = true;
            // 
            // rdbCinco
            // 
            this.rdbCinco.AutoSize = true;
            this.rdbCinco.Location = new System.Drawing.Point(82, 154);
            this.rdbCinco.Name = "rdbCinco";
            this.rdbCinco.Size = new System.Drawing.Size(58, 17);
            this.rdbCinco.TabIndex = 29;
            this.rdbCinco.TabStop = true;
            this.rdbCinco.Text = "CINCO";
            this.rdbCinco.UseVisualStyleBackColor = true;
            // 
            // rdbCuatro
            // 
            this.rdbCuatro.AutoSize = true;
            this.rdbCuatro.Location = new System.Drawing.Point(82, 126);
            this.rdbCuatro.Name = "rdbCuatro";
            this.rdbCuatro.Size = new System.Drawing.Size(70, 17);
            this.rdbCuatro.TabIndex = 28;
            this.rdbCuatro.TabStop = true;
            this.rdbCuatro.Text = "CUATRO";
            this.rdbCuatro.UseVisualStyleBackColor = true;
            // 
            // rdbTres
            // 
            this.rdbTres.AutoSize = true;
            this.rdbTres.Location = new System.Drawing.Point(82, 100);
            this.rdbTres.Name = "rdbTres";
            this.rdbTres.Size = new System.Drawing.Size(54, 17);
            this.rdbTres.TabIndex = 27;
            this.rdbTres.TabStop = true;
            this.rdbTres.Text = "TRES";
            this.rdbTres.UseVisualStyleBackColor = true;
            // 
            // rdbDos
            // 
            this.rdbDos.AutoSize = true;
            this.rdbDos.Location = new System.Drawing.Point(82, 74);
            this.rdbDos.Name = "rdbDos";
            this.rdbDos.Size = new System.Drawing.Size(48, 17);
            this.rdbDos.TabIndex = 26;
            this.rdbDos.TabStop = true;
            this.rdbDos.Text = "DOS";
            this.rdbDos.UseVisualStyleBackColor = true;
            // 
            // rdbUno
            // 
            this.rdbUno.AutoSize = true;
            this.rdbUno.Location = new System.Drawing.Point(82, 48);
            this.rdbUno.Name = "rdbUno";
            this.rdbUno.Size = new System.Drawing.Size(49, 17);
            this.rdbUno.TabIndex = 25;
            this.rdbUno.TabStop = true;
            this.rdbUno.Text = "UNO";
            this.rdbUno.UseVisualStyleBackColor = true;
            // 
            // frmJuego
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 552);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.grbMesa);
            this.Name = "frmJuego";
            this.Text = "GENERALA";
            this.Load += new System.EventHandler(this.frmJuego_Load);
            this.grbMesa.ResumeLayout(false);
            this.grbMesa.PerformLayout();
            this.gbSeleccion.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnLanzar;
        private System.Windows.Forms.Button btnDado2;
        private System.Windows.Forms.Button btnDado3;
        private System.Windows.Forms.Button btnDado4;
        private System.Windows.Forms.Button btnDado5;
        private System.Windows.Forms.GroupBox grbMesa;
        private System.Windows.Forms.Label lblTirosDisponibles;
        private System.Windows.Forms.Label lblDadosSeleccionados;
        private System.Windows.Forms.GroupBox gbSeleccion;
        private System.Windows.Forms.Button btnDadoSel4;
        private System.Windows.Forms.Button btnDadoSel5;
        private System.Windows.Forms.Button btnDadoSel3;
        private System.Windows.Forms.Button btnDadoSel1;
        private System.Windows.Forms.Button btnDadoSel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdbGenerala;
        private System.Windows.Forms.RadioButton rdbPoker;
        private System.Windows.Forms.RadioButton rdbFull;
        private System.Windows.Forms.RadioButton rdbEscalera;
        private System.Windows.Forms.RadioButton rdbSeis;
        private System.Windows.Forms.RadioButton rdbCinco;
        private System.Windows.Forms.RadioButton rdbCuatro;
        private System.Windows.Forms.RadioButton rdbTres;
        private System.Windows.Forms.RadioButton rdbDos;
        private System.Windows.Forms.RadioButton rdbUno;
        private System.Windows.Forms.TextBox txtGenerala;
        private System.Windows.Forms.TextBox txtPoker;
        private System.Windows.Forms.TextBox txtFull;
        private System.Windows.Forms.TextBox txtEscalera;
        private System.Windows.Forms.TextBox txtSeis;
        private System.Windows.Forms.TextBox txtCinco;
        private System.Windows.Forms.TextBox txtCuatro;
        private System.Windows.Forms.TextBox txtTres;
        private System.Windows.Forms.TextBox txtDos;
        private System.Windows.Forms.TextBox txtUno;
        private System.Windows.Forms.TextBox txtGeneralaDoble;
        private System.Windows.Forms.RadioButton rdbGeneralaDoble;
        private System.Windows.Forms.Button btnAnotar;
        private System.Windows.Forms.Button btnDado1;
    }
}