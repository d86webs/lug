USE [dbSueldos]
GO
/****** Object:  Table [dbo].[tb_anos]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_anos](
	[id_ano] [int] NOT NULL,
	[ano] [int] NOT NULL,
 CONSTRAINT [PK_tb_anos] PRIMARY KEY CLUSTERED 
(
	[id_ano] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_conceptos]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_conceptos](
	[id_concepto] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[porcentaje] [float] NOT NULL,
 CONSTRAINT [PK_tb_conceptos] PRIMARY KEY CLUSTERED 
(
	[id_concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_empleados]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_empleados](
	[legajo] [int] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[apellido] [varchar](50) NOT NULL,
	[cuil] [varchar](13) NOT NULL,
	[fec_alta] [date] NOT NULL,
 CONSTRAINT [PK_tb_empleados] PRIMARY KEY CLUSTERED 
(
	[legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_meses]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_meses](
	[id_mes] [int] NOT NULL,
	[mes] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tb_meses] PRIMARY KEY CLUSTERED 
(
	[id_mes] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_recibos]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_recibos](
	[id_recibo] [int] NOT NULL,
	[legajo] [int] NOT NULL,
	[id_mes] [int] NOT NULL,
	[id_ano] [int] NOT NULL,
	[bruto] [numeric](18, 2) NOT NULL,
	[descuento] [numeric](18, 2) NOT NULL,
	[neto] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_tb_recibos] PRIMARY KEY CLUSTERED 
(
	[id_recibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tb_recibos]  WITH CHECK ADD  CONSTRAINT [FK_tb_recibos_tb_empleados1] FOREIGN KEY([legajo])
REFERENCES [dbo].[tb_empleados] ([legajo])
GO
ALTER TABLE [dbo].[tb_recibos] CHECK CONSTRAINT [FK_tb_recibos_tb_empleados1]
GO
/****** Object:  StoredProcedure [dbo].[sp_anos_listar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_anos_listar]
as
begin
	select * from tb_anos order by ano desc
end
GO
/****** Object:  StoredProcedure [dbo].[sp_conceptos_eliminar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_conceptos_eliminar]
	@id int
as
begin

	delete from tb_conceptos where id_concepto = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_conceptos_insertar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_conceptos_insertar]
	@nombre varchar(50),
	@porcentaje float
as
begin

	declare @id int
	set @id = isnull((select max(id_concepto) from tb_conceptos), 0) + 1

	insert into tb_conceptos values (@id, @nombre, @porcentaje)

end
GO
/****** Object:  StoredProcedure [dbo].[sp_conceptos_listar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_conceptos_listar]
as
begin

	select * from tb_conceptos

end
GO
/****** Object:  StoredProcedure [dbo].[sp_conceptos_modificar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_conceptos_modificar]
	@id int,
	@nombre varchar(50),
	@porcentaje float
as
begin

	update tb_conceptos set nombre = @nombre, porcentaje = @porcentaje where id_concepto = @id

end
GO
/****** Object:  StoredProcedure [dbo].[sp_empleados_eliminar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_empleados_eliminar]
	@legajo int
as
begin
	
	delete from tb_empleados where legajo = @legajo

end
GO
/****** Object:  StoredProcedure [dbo].[sp_empleados_insertar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_empleados_insertar]
	@nombre varchar(50),
	@apellido varchar(50),
	@cuil varchar(13),
	@fec_alta date
as
begin
	
	declare @legajo int
	set @legajo = isnull((select max(legajo) from tb_empleados), 0) + 1

	insert into tb_empleados values (@legajo, @nombre, @apellido, @cuil, @fec_alta)

end
GO
/****** Object:  StoredProcedure [dbo].[sp_empleados_listar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_empleados_listar]
as
begin
	
	select * from tb_empleados

end
GO
/****** Object:  StoredProcedure [dbo].[sp_empleados_modificar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_empleados_modificar]
	@legajo int,
	@nombre varchar(50),
	@apellido varchar(50),
	@cuil varchar(13),
	@fec_alta date
as
begin
	
	update tb_empleados set nombre = @nombre, apellido = @apellido, cuil = @cuil, fec_alta = @fec_alta where legajo = @legajo

end
GO
/****** Object:  StoredProcedure [dbo].[sp_meses_listar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[sp_meses_listar]
as
begin
	select * from tb_meses order by id_mes
end
GO
/****** Object:  StoredProcedure [dbo].[sp_recibo_insertar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_recibo_insertar]
	@legajo int,
	@mes int,
	@ano int,
	@bruto numeric(18,2),
	@neto numeric(18,2),
	@descuento numeric(18,2)
as
begin

	declare @id int
	set @id = isnull((select max(id_recibo) from tb_recibos), 0) + 1

	insert into tb_recibos values (@id, @legajo, @mes, @ano, @bruto, @descuento, @neto)

end
GO
/****** Object:  StoredProcedure [dbo].[sp_recibo_listar]    Script Date: 29/09/2020 19:32:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[sp_recibo_listar]
as
begin
	select * from tb_recibos order by id_recibo
end
GO
