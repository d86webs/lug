﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Escala
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private int id_vuelo;

        public int Id_vuelo
        {
            get { return id_vuelo; }
            set { id_vuelo = value; }
        }

        private int id_destino;

        public int Id_destino
        {
            get { return id_destino; }
            set { id_destino = value; }
        }
    }
}
