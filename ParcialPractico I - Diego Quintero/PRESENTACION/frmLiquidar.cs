﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PRESENTACION
{
    public partial class frmLiquidar : Form
    {
        BLL.Empleado gestorEmpleados = new BLL.Empleado();
        BLL.Ano gestorAnos = new BLL.Ano();
        BLL.Mes gestorMeses = new BLL.Mes();
        BLL.Concepto gestorConceptos = new BLL.Concepto();
        BLL.Recibo gestorRecibos = new BLL.Recibo();
        public frmLiquidar()
        {
            InitializeComponent();
        }

        private void frmLiquidar_Load(object sender, EventArgs e)
        {
            EnlazarCombos();
            txtSueldoBasico.Enabled = true;
        }

        void EnlazarCombos()
        {
            cboNombreEmpleado.Items.AddRange(gestorEmpleados.ListarEmpleados().ToArray());
            cboAno.Items.AddRange(gestorAnos.ListarAnos().ToArray());
            cboMes.Items.AddRange(gestorMeses.ListarMeses().ToArray());
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            if(cboNombreEmpleado.SelectedIndex == -1 || cboAno.SelectedIndex == -1 || cboMes.SelectedIndex == -1 || string.IsNullOrWhiteSpace(txtSueldoBasico.Text))
            {
                MessageBox.Show("Falta completar uno o mas campos!");
            }
            else
            {
                txtSueldoBasico.Enabled = false;
                EnlazarConceptos();
                Calcular(decimal.Parse(txtSueldoBasico.Text));
            }
        }

        void EnlazarConceptos()
        {
            dgConceptos.DataSource = null;
            dgConceptos.DataSource = gestorConceptos.ListarConceptos();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtSueldoBasico.Text))
            {
                MessageBox.Show("Debe completar Sueldo Basico");
            }
            else
            {
                txtSueldoBasico.Enabled = true;
                dgConceptos.DataSource = null;
                txtSueldoBruto.Clear();
                txtDescuentos.Clear();
                txtSueldoNeto.Clear();
            }
        }

        void Calcular(decimal sueldoBasico)
        {
            decimal sueldoBruto = sueldoBasico, descuentos = 0, sueldoNeto = 0;

            foreach(BE.Concepto c in gestorConceptos.ListarConceptos())
            {
                if(c.Porcentaje > 0)
                {
                    sueldoBruto += sueldoBasico * (c.Porcentaje / 100);
                }
                else if(c.Porcentaje < 0)
                {
                    descuentos -= sueldoBasico * (c.Porcentaje / 100);
                }
            }

            sueldoNeto = sueldoBruto - descuentos;

            txtSueldoBruto.Text = sueldoBruto.ToString();
            txtDescuentos.Text = descuentos.ToString();
            txtSueldoNeto.Text = sueldoNeto.ToString();
        }

        private void btnLiquidar_Click(object sender, EventArgs e)
        {
            BE.Empleado empleado = (BE.Empleado)cboNombreEmpleado.SelectedItem;
            BE.Ano ano = (BE.Ano)cboAno.SelectedItem;
            BE.Mes mes = (BE.Mes)cboMes.SelectedItem;
            
            BE.Recibo r = new BE.Recibo();
            r.Legajo = empleado.Legajo;
            r.Mes = mes.Id;
            r.Ano = ano.Id;
            r.Bruto = decimal.Parse(txtSueldoBruto.Text);
            r.Neto = decimal.Parse(txtSueldoNeto.Text);
            r.Descuento = decimal.Parse(txtDescuentos.Text);
            gestorRecibos.Insertar(r);
            
            MessageBox.Show("Liquidacion Exitosa");
            Limpiar();
;       }

        void Limpiar()
        {
            cboNombreEmpleado.SelectedIndex = -1;
            cboAno.SelectedIndex = -1;
            cboMes.SelectedIndex = -1;
            dgConceptos.DataSource = null;
            txtSueldoBasico.Clear();
            txtSueldoBruto.Clear();
            txtDescuentos.Clear();
            txtSueldoNeto.Clear();
            txtSueldoBasico.Enabled = true;
        }

        private void frmLiquidar_Activated(object sender, EventArgs e)
        {
            EnlazarCombos();
        }
    }
}
