﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace BLL
{
    public class Pasaje
    {
        DAL.MP_Pasaje mp = new DAL.MP_Pasaje();

        public void Insertar(BE.Pasaje p)
        {
            mp.Insertar(p);
        }

        //public List<BE.Pasaje> ListarPasajes()
        //{
        //    return mp.Listar();
        //}

        public DataTable Listar()
        {
            return mp.Listar();
        }
    }
}
